# Loveternational Web App

## Prequisites (Development):

| Module | Version |
| --- | --- |
| Node | 8.15.1|
| Npm | 6.4.1 |
| Angular-cli |  7.3.7 |
| Angular | 7.3.7 |


------------
##### Structure
      Loveternational Web App
        |- ng-front
            |- dev
            |- staging
       
##### Take Clone of project

$ git clone -b branch_name git_url folder_name

##### Development Process

$cd folder_name

$ npm install

#### rename configs-sample.json to configs.json

$ cd src/assets/configs

$ mv configs-sample.json configs.json

$ ng serve

$ ng build --prod


------------

### Deployment Process
Open Indianic Server 

$ git pull origin master

$ pm2 start server.js --name="project_name"

restart pm2 instance:

$ pm2 restart "project_instance"

------------

