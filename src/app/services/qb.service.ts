import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { Observable, of } from 'rxjs';
import { error } from '@angular/compiler/src/util';

@Injectable({
  providedIn: 'root'
})
export class QBService {

  _notificationObj = new EventEmitter<any>();

  constructor(private http: HttpClient) { }

  createDialog(token, occupantId) {
    const body = {
      'type': 3,
      'occupants_ids': occupantId
    };
    return this.http.post('https://api.quickblox.com/chat/Dialog.json',
      body,
      {
        headers: { 'QB-Token': token, 'content-type': 'application/json' }
      });
  }

  retriveMessage(token, dialogId) {
    return this.http.get('https://api.quickblox.com/chat/Message.json?chat_dialog_id=' + dialogId + '&sort_asc=date_sent', {
      headers: { 'QB-Token': token }
    });
  }

  getUsers(token, id) {
    return this.http.get('https://api.quickblox.com/users.json?filter[]=number+id+ne+' + id + '&order=desc%20date%20updated_at' + '&per_page=50', {
      headers: { 'QB-Token': token, 'content-type': 'application/json' }
    });
  }


  // getUsers(token, id) {
  //   return this.http.get('https://api.quickblox.com/users.json', {
  //     headers: { 'QB-Token': token, 'content-type': 'application/json' }
  //   });
  // }

  getProfileDetails(uname, token) {
    return this.http.get('https://api.quickblox.com/users/by_login.json?login=' + uname + '&order=desc%20date%20updated_at', {
      headers: { 'QB-Token': token, 'content-type': 'application/json' }
    });
  }
  sendAttachment(file, token, fd) {
    // let params : any = { name: file.name, content_type: file.type, size: file.size};
    return this.http.post('https://api.quickblox.com/blobs.json', fd, {
      headers: { 'QB-Token': token }
    });
  }




  getDialog(token) {
    return this.http.get('https://api.quickblox.com/chat/Dialog.json?sort_desc=last_message_date_sent', {
      headers: { 'QB-Token': token, 'content-type': 'application/json' }
    });
  }

  getAttachment(token, id) {
    return this.http.get('https://api.quickblox.com/blobs/' + id + '.json', {
      headers: { 'QB-Token': token, 'content-type': 'application/json' }
    });
  }

  uploadMessage(token, data) {
    return this.http.post('https://api.quickblox.com/chat/Message.json', data, {
      headers: { 'QB-Token': token }
    });
  }

  uploadFile(url, data) {
    return this.http.post(url, data).map((data) => {
      // console.log("data==", data);
    }, (error) => {
      // console.log("hello", error);
    });
    ;
  }

  // sendAttachment(file, token) {
  //   return this.http.post('https://api.quickblox.com/blobs.json', file,{
  //     headers: { 'QB-Token': token }
  //   });
  // }
}
