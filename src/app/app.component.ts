import { Component, OnInit, Injector } from '@angular/core';
import { TransferState, makeStateKey, Title, Meta } from '@angular/platform-browser';
import { BaseComponent } from './common/commonComponent';
declare var $: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends BaseComponent implements OnInit {
  public inValidMsg;
  public inValidMsgStatus;
  public noData: boolean = false;
  screenLoaderFlag = false;
  removescreenLoaderFlag = false;
  constructor(
    public titleService: Title,
    public metaService: Meta,
    inj: Injector,
  ) {
    super(inj);

  }
  ngOnDestroy() {
    this.inValidMsg.unSubscribe()
  }
  ngOnInit() {
    this.titleService.setTitle('Loveternational');
    this.metaService.addTag({ name: 'description', content: 'Shipping Made Simple' });
    this.screenLoaderFlag = false;
    this.removescreenLoaderFlag = false;
    $('body').addClass('body-loader');
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      $('body').removeClass('body-loader');
    }, 4000);
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.removescreenLoaderFlag = true;
    }, 4000);
    setTimeout(() => {
      /** spinner ends after 3 seconds */
      this.screenLoaderFlag = true;
    }, 3000);
    this.qb.init(this.qbAccount.appId, this.qbAccount.authKey, this.qbAccount.authSecret, this.qbEndpoints);

  }
} 
