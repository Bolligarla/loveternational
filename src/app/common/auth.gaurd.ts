import { Injectable, Injector, APP_ID, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { BaseComponent } from './../common/commonComponent';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';

/****************************************************************************
@PURPOSE      : To allow public pages can be activated. (After Login)
@PARAMETERS   : N/A
@RETURN       : <boolean>
/****************************************************************************/

@Injectable()
export class CanLoginActivate extends BaseComponent implements CanActivate {
  profile: string;
  constructor(inj: Injector) { super(inj) }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!this.getToken("accessToken")) {
      return true;
    }
    this.profile = this.getToken('is_profile_complete');
    this.router.navigate(['/main/dashboard']);
    return false
  }
}
/****************************************************************************/

/****************************************************************************
@PURPOSE      : To allow authorized pages can be activated. (Before Login)
@PARAMETERS   : N/A
@RETURN       : <boolean>
/****************************************************************************/
@Injectable()
export class CanAuthActivate extends BaseComponent implements CanActivate {
  constructor(inj: Injector) { super(inj) }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.getToken("accessToken")) {
      return true;
    }
    this.router.navigate(['/login']);
    return false
  }
}
/****************************************************************************/



