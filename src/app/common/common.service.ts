import { Component, PLATFORM_ID, Injectable, NgZone, APP_ID, Inject } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { BaseComponent } from './../common/commonComponent';
import { HttpClient, HttpHeaders, HttpResponse, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { Broadcaster } from './broadcaster';
import { Router } from "@angular/router";
import * as config from 'assets/config/configs.json';
import swal from 'sweetalert2'

@Injectable({
	providedIn: 'root'
})

export class CommonService {
	notiSubscription = false;
	authorised: any = false;
	subscription: any;
	imagePath: any;
	constructor(public spinner: NgxSpinnerService, public broadcaster: Broadcaster, public router: Router, public _http: HttpClient, @Inject(PLATFORM_ID) platformId: Object) {
		this.platformId = platformId;
		this._apiUrl = this.config.apiUrl;
		this._imageUrl = this.config.imageUrl;
		this._socketUrl = this.config.socketUrl;
		this.imagePath = this.config.imagePath;

	}
	offlineStatus = false;
	isConnected = true;
	public config = (<any>config)
	public _socketUrl;
	public _apiUrl;
	public _imageUrl;
	public platformId;


	public getToken(key) {
		if (isPlatformBrowser(this.platformId)) {
			return window.localStorage.getItem(key);
		}
	}
	public setToken(key, value) {
		if (isPlatformBrowser(this.platformId)) {
			window.localStorage.setItem(key, value);
		}
	}


	/*******************************************************************************************
@PURPOSE      	: 	Call api.
@Parameters 	: 	{
						url : <url of api>
						data : <data object (JSON)>
						method : String (get, post)
						isForm (Optional) : Boolean - to call api with form data
						isPublic (Optional) : Boolean - to call api without auth header
					}
/*****************************************************************************************/
	callApi(url, data, method, isForm?, isPublic?): Promise<any> {
		let headers;
		if (isPublic) {
			headers = new HttpHeaders({ 'content-Type': 'application/json' });
		} else {
			headers = new HttpHeaders({ 'content-Type': 'application/json', 'Authorization': this.getToken('accessToken') });
		}
		if (isForm) {
			headers = new HttpHeaders({ 'Authorization': this.getToken('accessToken') });
		}
		return new Promise((resolve, reject) => {
			if (method == 'post') {
				this._http.post(this._apiUrl + 'api/' + url, data, { headers })
					.subscribe(data => {
						resolve(data)
					}, error => {
						this.showServerError(error)
					})


			} else if (method == 'get') {
				// let params: { appid: 'id1234', cnt: '5' }
				this._http.get(this._apiUrl + 'api/' + url, { headers })
					.subscribe(data => { resolve(data) }, error => { this.showServerError(error) })
			}
		})
	}

	getPeople(term: string = null, fieldName): Observable<any[]> {
		let data = {
			filter: {}
		}
		data.filter[fieldName] = term
		let items = []
		if (term.length > 2) {
			this.callApi('search', data, 'post').then(success => {
				success.data.firstName.forEach(a => {
					items.push({ name: a })
				})
			})
		} else {
			items = []
		}

		return of(items).pipe(delay(500));
	}

	/*****************************************************************************************/
	// @PURPOSE      	: 	To show server error
	/*****************************************************************************************/
	public swal = swal;
	public invalidMsg;

	showServerError(error) {

		if (error.status == 401) {
			this.invalidMsg = error.error.settings.message
			this.logout();
			this.spinner.hide()
			// this.broadcaster.broadcast('invalidMsg', this.invalidMsg)
		}
		if (error.status == 0) {
			this.spinner.hide()
		}

	}

	logout() {
		if (isPlatformBrowser(this.platformId)) {
			window.localStorage.clear();
			sessionStorage.clear();
		}
		this.router.navigate(['/sign-in']);
	}

}


