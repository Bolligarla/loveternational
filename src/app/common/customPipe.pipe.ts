import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
import * as moment from 'moment';


@Pipe({
	name: 'sanitizeHtml'
})

@Injectable()
export class CustomPipe implements PipeTransform {
	constructor(protected sanitizer: DomSanitizer) { }
	transform(value: any, type: string): SafeHtml | SafeStyle | SafeScript | SafeUrl | SafeResourceUrl {
		switch (type) {
			case 'html': return this.sanitizer.bypassSecurityTrustHtml(value);
			case 'style': return this.sanitizer.bypassSecurityTrustStyle(value);
			case 'script': return this.sanitizer.bypassSecurityTrustScript(value);
			case 'url': return this.sanitizer.bypassSecurityTrustUrl(value);
			case 'resourceUrl': return this.sanitizer.bypassSecurityTrustResourceUrl(value);
			default: throw new Error(`Invalid safe type specified: ${type}`);
		}
	}
}

@Pipe({
	name: 'ConvertTime'
})

@Injectable()
export class CustomTime implements PipeTransform {
	transform(value: any): any {
		var newDate = moment(new Date());
		var oldDate = moment(new Date(value));
		var difference = newDate.diff(oldDate, 'days');
		// if (difference === 0) {
		return moment(value).local().format('HH:mm');
		// }else if(difference === 1) {
		// 	return "Yesterday"
		// }else if(difference >= 2 && difference <= 6) {
		// 	return moment(value).format("ddd");
		// } else {
		// 	return moment(value).format("MM-DD-YYYY");
		// }

	}
}

@Pipe({
	name: 'checkDate'
})

@Injectable()
export class checkDate implements PipeTransform {
	transform(value: any): any {
		var newDate = moment(new Date());
		var oldDate = moment(new Date(value));
		var difference = newDate.diff(oldDate, 'days');
		if (difference === 0) {
			return "Today"
		} else if (difference === 1) {
			return "Yesterday"
		} else {
			return moment(value).format("Do MMMM YYYY")
		}
	}


}

@Pipe({
	name: 'limitText'
})
export class limitTextPipe implements PipeTransform {


	transform(string: any, length: any): any {
		// let dump1 = string;
		if (string && string.length > length) {
			return string.substr(0, length) + "...";
		}
		return string;
		// return dump1;
	}
}
