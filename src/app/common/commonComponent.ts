import { Component, OnInit, PLATFORM_ID, Injector, NgZone, APP_ID } from '@angular/core';
import { TransferState, makeStateKey, Title, Meta } from '@angular/platform-browser';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { CommonService } from './common.service';
import { ErrorMessages } from './errorMessages';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';
import { NgxSpinnerService } from 'ngx-spinner';
import swal from 'sweetalert2';
import { Broadcaster } from './broadcaster';
import * as moment from 'moment';
// import * as qb from 'quickblox';
import { ConnectionService } from 'ng-connection-service';
import { QBService } from './../services/qb.service';
import * as qbconfig from '../../assets/config/qb.config';
declare var QB: any;
declare var window: any;
declare var Push: any;
import * as JWT from 'jwt-decode';
import { Observable } from 'rxjs/Observable';
import * as socketIo from 'socket.io-client';
@Component({
  selector: 'parent-comp',
  template: ``,
  providers: []
})

export class BaseComponent {

  constructor(injector: Injector) {
    this.router = injector.get(Router)
    this.platformId = injector.get(PLATFORM_ID)
    this.appId = injector.get(APP_ID)
    this.commonService = injector.get(CommonService)
    this.qBService = injector.get(QBService)
    this.errorMessage = injector.get(ErrorMessages)
    this.http = injector.get(HttpClient)
    this.titleService = injector.get(Title)
    this.metaService = injector.get(Meta)
    this.activatedRoute = injector.get(ActivatedRoute)
    this.baseUrl = this.commonService._apiUrl;
    this.socketUrl = this.commonService._socketUrl;
    this.spinner = injector.get(NgxSpinnerService);
    this.broadcaster = injector.get(Broadcaster);
    this.connectionService = injector.get(ConnectionService);
    this.qbEndpoints = qbconfig.qbEndpoints;
    this.qbAccount = qbconfig.qbAccount;
    this.imagePath = this.commonService.imagePath;
    if (this.getToken('accessToken')) {
      this.decodeToken = JWT(this.getToken('accessToken'));
    }
    this.connectionService.monitor().subscribe(isConnected => {
      this.commonService.isConnected = isConnected;
      if (!this.commonService.isConnected) {
        if (!this.commonService.offlineStatus) {
          this.status1 = "You are not connected to internet"
        }
        setTimeout(() => {
          this.status1 = '';
        }, 1000)
        this.commonService.offlineStatus = true;


      }
      else {
        this.status1 = "";
        this.commonService.offlineStatus = false;
      }
    })
    this.router.events.subscribe(event => {

      if (event instanceof NavigationEnd) {
        this.commonService.offlineStatus = false;
        if (!this.commonService.isConnected) {
          if (!this.commonService.offlineStatus) {
            this.status1 = "You are not connected to internet"
          }
          setTimeout(() => {
            this.status1 = '';
          }, 1000)
          this.commonService.offlineStatus = true;



        }
        if (!this.getToken('accessToken')) {
          this.commonService.notiSubscription = false;
        }
        if (this.getToken('accessToken') && !this.commonService.notiSubscription) {
          this.triggerPush();
        }
      }
    })



  }
  public connectionService: ConnectionService;
  public Verticalscrollbar = { axis: 'y', theme: 'light' };
  public horizontalscrollbar = { axis: 'x', theme: 'light' };
  public activatedRoute: ActivatedRoute;
  public errorMessage: ErrorMessages
  public swal = swal;
  public titleService: Title
  public metaService: Meta
  public platformId: any;
  public appId: any;
  public http: any;
  public router: Router;
  public commonService: CommonService;
  public qBService: QBService;
  public baseUrl;
  public socketUrl;
  public spinner: NgxSpinnerService;
  public broadcaster: Broadcaster;
  public moment: any = moment;
  public status1: any;
  public qbAccount: any;
  public qbEndpoints: any;
  public qb: any = QB;
  public decodeToken: any;
  public publicVapidKey = 'BNoJYqtlHxMOLFbN6Mv0BIvZjbM72t41Veg2N5np67TIjpIugGrhQ-8qWUzSkL9UMTWU8PZimRto8Mk5DCRqWbQ';
  private socket;
  public observer;
  public imagePath;
  ngOnInit() {

  }
  ngAfterViewInit() {
    $(".datatable-columnsorting .dropdown-menu").click(function (e) {
      e.stopPropagation();
    });


  }
  // *************************************************************//
  //@Purpose : We can use following function to use localstorage
  //*************************************************************//
  setToken(key, value) {
    if (isPlatformBrowser(this.platformId)) {
      window.localStorage.setItem(key, value);
    }
  }
  getToken(key) {
    if (isPlatformBrowser(this.platformId)) {
      return window.localStorage.getItem(key);
    }
  }
  removeToken(key) {
    if (isPlatformBrowser(this.platformId)) {
      window.localStorage.removeItem(key);
    }
  }
  clearToken() {
    if (isPlatformBrowser(this.platformId)) {
      window.localStorage.clear()
    }
  }
  //*************************************************************//

  //*************************************************************//
  //@Purpose : We can use following function to use Toaster Service.
  //*************************************************************//
  popToast(type, title, message?, confirm?) {
    if (!confirm) {
      confirm = false;
    }
    if (!message) {
      swal({
        position: 'center',
        type: type,
        text: title,
        showConfirmButton: confirm,
        timer: 5000,
        customClass: 'custom-toaster'
      })
    } else {
      swal({
        position: 'center',
        type: type,
        title: title,
        text: message,
        showConfirmButton: confirm,
        customClass: 'custom-toaster'
      }).then((result) => {
      })
    }
  }

  /****************************************************************************
  @PURPOSE      : To restrict or allow some values in input.
  @PARAMETERS   : $event
  @RETURN       : Boolen
  ****************************************************************************/
  RestrictSpace(e) {
    if (e.keyCode == 32) {
      return false;
    } else {
      return true;
    }
  }

  AllowNumbers(e) {
    var input;
    if (e.metaKey || e.ctrlKey) {
      return true;
    }
    if (e.which === 32) {
      return false;
    }
    if (e.which === 0) {
      return true;
    }
    if (e.which < 33) {
      return true;
    }
    if (e.which === 43 || e.which === 45) {
      return true;
    }
    input = String.fromCharCode(e.which);
    return !!/[\d\s]/.test(input);
  }

  AllowChar(e) {
    if ((e.keyCode > 64 && e.keyCode < 91) || (e.keyCode > 96 && e.keyCode < 123) || e.keyCode == 8) {
      return true
    } else {
      return false
    }
  }
  /****************************************************************************/

  /****************************************************************************
  @PURPOSE      : To show validation message
  @PARAMETERS   : <field_name, errorObj?>
  @RETURN       : error message.
  ****************************************************************************/
  showError(field, errorObj?) {
    return this.errorMessage.getError(field, errorObj)
  }
  /****************************************************************************/
  getProfile() {
    var url = this.getToken('profilePic');
    if (url == null || url == ' ') {
      return this.baseUrl + 'assets/images/no-image.jpg'
    } else {
      return url;
    }
  }
  public count = 0;
  gethomeImage() {

    if (this.count === 0) {
      return 'assets/images/white-logo.png'
    }
    this.count++;
  }
  // CustomScroll() {
  //     $(".verticalScroll").mCustomScrollbar({
  //         axis: "y", // Vertical scrollbar
  //         theme: "light", //light,
  //         scrollbarPosition: "outside"
  //     });
  //     $(".horizontalScroll").mCustomScrollbar({
  //         axis: "x", // horizontal scrollbar
  //         theme: "light" //light
  //     });
  //     if (/iPhone|iPod|iPad/i.test(navigator.userAgent)) {
  //         //for iPhone & iPod
  //         $(".horizontalScroll,.verticalScroll").mCustomScrollbar("destroy");
  //     }

  //     // Detect ios 11_x_x affected  
  //     // NEED TO BE UPDATED if new versions are affected
  //     var ua = navigator.userAgent,
  //         iOS = /iPad|iPhone|iPod/.test(ua),
  //         iOS11 = /OS 11_0|OS 11_1|OS 11_2/.test(ua);

  //     // ios 11 bug caret position
  //     if (iOS && iOS11) {

  //         // Add CSS class to body
  //         $("body").addClass("iosBugFixCaret");

  //     }
  // }
  /************************
       logout user
   **************************/
  logout1() {
    this.swal({
      title: 'Loveternational',
      text: 'Are you sure you want to logout?',
      cancelButtonText: 'NO',
      confirmButtonText: 'YES',
      allowOutsideClick: false,
      showCancelButton: true,
      buttonsStyling: false
    }).then((result) => {
      if (result.value == true) {
        this.commonService.callApi('logout', '', 'get', false, false).then(success => {
          if (success.settings.status == 1) {
            // sessionStorage.removeItem('session');
            // sessionStorage.removeItem('uname');
            // sessionStorage.removeItem('pass');
            this.removeToken('accessToken');
            this.removeToken('profile_url');
            this.removeToken('full_name');
            this.removeToken('transactionId');
            this.removeToken('intrestedFriendUserId');
            this.removeToken('intrestedFriendQBId');
            this.removeToken('intrestedFriendQBFullName');
            this.removeToken('intrestedFriendQBProfile')
            this.removeToken('__paypal_storage__');
            this.removeToken('fbId')
            this.router.navigate(["/sign-in"]);
            if (QB.chat) {
              QB.chat.disconnect();
            }
            sessionStorage.clear();
          }
        })
      }
    });
  }
  isSafari;
  iOS;
  async triggerPush() {
    this.commonService.notiSubscription = true;
    this.isSafari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);
    this.iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    if ((this.isSafari || this.iOS) && (navigator.vendor === 'Apple Computer, Inc.')) {
      if (!Push.Permission.has()) {
        this.showPushNotification('Hello', 'Test');
      }
      this.getSocketData('getToken', 'get_notifications', { token: this.getToken('accessToken') })
    } else {
      if ('serviceWorker' in navigator) {
        try {
          const register = await navigator.serviceWorker.register('assets/js/sw.js', {
            scope: 'assets/js/'
          });
          if (register.pushManager) {
            this.commonService.subscription = await register.pushManager.subscribe({
              userVisibleOnly: true,
              applicationServerKey: this.urlBase64ToUint8Array(this.publicVapidKey),
            });
            let data = {
              subscription: this.commonService.subscription
            }
            this.commonService.callApi('addUpdateDeviceId', data, 'post', true, true).then(success => {
            })
          }
        } catch (e) {
          this.commonService.subscription = { endpoint: "blocked" }
          let data = {
            subscription: this.commonService.subscription
          }
          this.commonService.callApi('addUpdateDeviceId', data, 'post', true, true).then(success => {
          })
        }
      } else {
      }
    }
  }
  showPushNotification(title, body) {
    Push.create(title, {
      body: body,
      icon: '/logo.png',
      timeout: 5000,
      onClick: function () {
        window.focus();
        this.close();
      }
    });
  }
  getSocketData(sendKey, getKey, data): Observable<any> {
    this.socket = socketIo(this.socketUrl);
    this.socket.emit('connection', {});
    this.socket.emit(sendKey, data);
    this.socket.on(getKey, (res) => {
      if (!Push.Permission.has()) {
        this.showPushNotification('Hello', 'Test');
      }
      if (this.iOS) {
        this.popToast('success', res.title, res.message, true)
      } else {
        this.showPushNotification(res.type, res.message);
      }
    });
    return this.getSocketDataObservable();
  }
  getSocketDataObservable(): Observable<any> {
    return new Observable(observer => {
      this.observer = observer;
    });
  }
  urlBase64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
      .replace(/-/g, '+')
      .replace(/_/g, '/');
    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);
    for (let i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
  }
}

