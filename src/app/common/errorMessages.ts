import { Injectable } from '@angular/core';

import * as messages from './errorMessages.json';

@Injectable({
  providedIn: 'root'
})

export class ErrorMessages {
  public MSG = (<any>messages)

  constructor() { }
  getError(field, error) {
    var message;
    if (error) {
      if (error.required) {
        var required = this.MSG.ERROR.REQUIRED;
        switch (field) {
          case 'email': { message = required.email } break;
          case 'mobile': { message = required.mobile } break;
          case 'password': { message = required.password } break;
          case 'oldPassword': { message = required.oldPassword } break;
          case 'newPassword': { message = required.newPassword } break;
          case 'confirmPassword': { message = required.confirmPassword } break;
          case 'firstname': { message = required.firstname } break;
          case 'fullname': { message = required.fullname } break;
          case 'lastname': { message = required.lastname } break;
          case 'username': { message = required.username } break;
          case 'Title': { message = required.Title } break;
          case 'content': { message = required.content } break;
          case 'metaTitle': { message = required.metaTitle } break;
          case 'metaKeyword': { message = required.metaKeyword } break;
          case 'metaDescription': { message = required.metaDescription } break;
          case 'pageId': { message = required.Pageid } break;
          case 'pageName': { message = required.pageName } break;
          case 'pageUrl': { message = required.pageUrl } break;
          case 'policyTerms': { message = required.policyTerms } break;
          case 'name_of_firm': { message = required.name_of_firm } break;
          case 'license_no': { message = required.license_no } break;
          case 'paypal_email_address': { message = required.paypal_email_address } break;
          case 'about_me': { message = required.about_me } break;
          case 'profile_url': { message = required.profile_url } break;
          case 'license_photo': { message = required.license_photo } break;
        }
      } else if (error.pattern) {
        var pattern = this.MSG.ERROR.PATTERN;
        switch (field) {
          case 'email': { message = pattern.email } break;
          case 'paypal_email_address': { message = pattern.paypal_email_address } break;

        }
      } else if (error.minlength) {
        var minlength = this.MSG.ERROR.MINLENGTH;
        switch (field) {
          case 'mobile': { message = minlength.mobile } break;
        }
      }
      return message;
    }
  }
}