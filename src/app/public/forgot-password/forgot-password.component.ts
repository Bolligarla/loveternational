import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent'

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent extends BaseComponent implements OnInit {

  public user: any = {};
  public submitted: boolean = false;
  public successMessageStatus: any;
  public successMessage: boolean = false;
  public errorMessageStatus: boolean = false;
  public errorMessageStatus1: any;
  public forgotPasswordValMsg: any;
  constructor(inj: Injector) {
    super(inj);
  }

  ngOnInit() {
  }

  /************************************
      forgot the passowrd
  *************************************/
  forgotPasswordMail(form, user) {
    if (!form.form.controls.email.valid) {
      this.submitted = true;
      this.errorMessageStatus = false;
      setTimeout(() => {
        this.submitted = false;
      }, 1000);
    }
    let data = {
      email: user.email
    }

    if (user.email == undefined) {
      this.forgotPasswordValMsg = "Please enter your email address"
      setTimeout(() => {
        this.forgotPasswordValMsg = "";
      }, 1000);
    }
    if (form.valid) {
      this.spinner.show();
      this.commonService.callApi('forgotPasswordMail', data, 'post', false, true).then(success => {
        if (success.settings.status == 1) {
          this.spinner.hide();
          if (!this.successMessage) {
            this.successMessageStatus = success.settings.message;
            this.successMessage = true;
            setTimeout(() => {
              this.successMessage = false;
            }, 1000);
          }
          setTimeout(() => {
            this.router.navigate(["/sign-in"]);
          }, 1000);
        }
        else {
          setTimeout(() => {
            this.spinner.hide();
          }, 1000);
          if (!this.errorMessageStatus) {
            this.errorMessageStatus1 = success.settings.message
            this.errorMessageStatus = true;
            setTimeout(() => {
              this.errorMessageStatus = false;
            }, 1000);
          }
        }
      })
    }
    else {
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);
    }
  }
}
