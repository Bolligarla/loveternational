import { Component, OnInit, Injector, Input } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent'
declare var $: any;
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent extends BaseComponent implements OnInit {
  public full_name: any;
  public menu: boolean;
  public email: any;
  public chatHide: boolean = false;
  public user_profile_url: any;
  public acc_type: any;
  public transactionId: any;
  @Input() type;
  constructor(inj: Injector) {
    super(inj)
  }
  ngDoCheck() {
    this.full_name = this.getToken('fullname');
    // this.user_profile_url = this.getToken('profile_url');
    if (this.type == "attorney") {
      if (this.decodeToken.data.acc_type == 'premium' || this.getToken('transactionId')) {
        this.chatHide = true;
      }
      else {
        this.chatHide = false;
      }
    }
  }
  ngOnInit() {
    if (this.type == "user") {
      this.menu = true;
      this.chatHide = true;
    }
    else {
      this.email = this.decodeToken.data.email;
      this.menu = false;
      if (this.decodeToken.data.acc_type == 'premium' || this.getToken('transactionId')) {
        this.chatHide = true;
      }
      else {
        this.chatHide = false;
      }
    }
    this.acc_type = this.decodeToken.data.acc_type;
    this.full_name = this.getToken('full_name');
    this.user_profile_url = this.getToken('profile_url');
  }
  CloseMenu() {
    $('body').removeClass('open-menu');
  }
}
