import { Component, OnInit, Injector } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { BaseComponent } from './../../common/commonComponent';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends BaseComponent implements OnInit {
  public customOptions: OwlOptions = {
    loop: false,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    },

    nav: true
  }
  public banners = [
    {
      id: 1, src: "assets/images/home-banner.png",
      alt: "image1", title: "Swiping up on the screen lets you see a user's profile details",
      icon: "assets/images/svg/white-up-arrow.svg"
    },
    {
      id: 2, src: "assets/images/home-banner.png",
      alt: "image2", title: "Swiping down on the screen will play a users's video greeting if any",
      icon: "assets/images/svg/white-down-arrow.svg"
    },
    {
      id: 3, src: "assets/images/home-banner.png",
      alt: "image3", title: "Swiping left on the screen will send a user a crush request",
      icon: "assets/images/svg/white-left-arrow.svg"
    },
    {
      id: 4, src: "assets/images/home-banner.png",
      alt: "image3",
      title: "Swipe right to return to the previous menu",
      icon: "assets/images/svg/white-right-arrow.svg"
    },
    {
      id: 5, src: "assets/images/home-banner.png",
      alt: "image3",
      title: "When a user accepts your crush request,you become mutual interests and unlock chat other functionality."
    },
    {
      id: 6, src: "assets/images/home-banner.png",
      alt: "image3",
      title: "Double tap on the user picture to cycle all user's uploaded pictures."
    },
  ]
  constructor(inj: Injector) {
    super(inj);
    setTimeout(() => {
      const devicename = navigator.userAgent.toLowerCase();
      const isAndroid = devicename.indexOf("android") > -1;
      const localFlag = this.getToken('isAndroid');
      if (isAndroid && !localFlag) {
        this.swal({
          title: 'Loveternational',
          text: 'Download our app on Google play',
          cancelButtonText: 'Cancel',
          confirmButtonText: 'Get',
          allowOutsideClick: false,
          showCancelButton: true,
          buttonsStyling: false
        }).then((result) => {
          if (result.value == true) {
            window.location.href = 'https://play.google.com/store/apps/details?id=com.loveternational&hl=en';
            this.setToken('isAndroid', true)
          }
          else {
            this.setToken('isAndroid', true)
          }
        })
      }
    }, 4000);
  }
  ngOnInit() {
  }
}
