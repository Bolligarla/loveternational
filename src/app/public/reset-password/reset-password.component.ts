import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from '../../common/commonComponent'

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent extends BaseComponent implements OnInit {

  public email: any;

  constructor(inj: Injector) {
    super(inj);
    this.email = this.decodeToken.data.email;
  }


  ngOnInit() {
  }


}

