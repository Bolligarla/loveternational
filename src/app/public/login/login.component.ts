import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';
import * as JWT from 'jwt-decode';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent extends BaseComponent implements OnInit {
  public submitted: boolean = false;
  public submitted1: boolean = false;
  public errorMessageStatus: boolean = false;
  public errorMessageStatus1: any;
  public user: any = {};
  public profileComplete: any;
  public role: any;
  public emailIdMsg: any;
  public passwordValMsg: any;
  public emailValMsg: any;
  public subscriptionData: any;
  constructor(inj: Injector) { super(inj) }
  ngOnInit() {
    if (this.getToken('inic_id') != null && this.getToken('inic_pass') != null) {
      this.user = {
        email: this.getToken('inic_id'),
        password: this.getToken('inic_pass'),
        rememberMe: true
      }
    }
  }
  //***********************************************************************//
  //@PURPOSE   : API call for login
  //***********************************************************************//
  onLogin(form, user) {
    if (!form.form.controls.email.valid) {
      this.submitted1 = true;
      this.errorMessageStatus = false;
      setTimeout(() => {
        this.submitted1 = false;
      }, 1000);
    }
    else if (user.password == undefined) {
      this.passwordValMsg = "Please enter your password"
      setTimeout(() => {
        this.passwordValMsg = "";
      }, 1000);
    }

    else if (form.valid && user.email.indexOf("@")) {
      if (this.commonService.subscription == undefined) {
        this.subscriptionData = { endpoint: "blocked" }
      }
      else {
        this.subscriptionData = this.commonService.subscription
      }
      let data = {
        email: user.email.toLowerCase(),
        password: user.password,
        device_type: "web",
        subscription: this.subscriptionData
      }
      this.spinner.show();
      this.commonService.callApi('userLogin', data, 'post', false, true).then(success => {
        if (success.status == 1) {
          setTimeout(() => {
            this.spinner.hide();
          });
          this.profileComplete = success.data.is_profile_complete;
          // this.qb.createSession({ email: user.email, password: success.data.qbps }, (error, response) => {
          //   if (response) {
          //     var params = { email: user.email, password: success.data.qbps };
          //     this.qb.login(params, (err, result) => {
          //       if (result.id) {
          //         this.spinner.hide();
          //         sessionStorage.setItem('session', JSON.stringify(response));
          //         // sessionStorage.setItem('pass', success.data.qbps);
          //         sessionStorage.setItem('uname', user.email);
          //       }
          //     });
          //   } else {
          //   }
          // });
          // if (success.data.profile_url) {
          //   this.setToken('profile_url', success.data.profile_url)
          // }
          // this.setToken('full_name', success.data.full_name)
          // this.setToken('accessToken', success.data.token)
          // if (this.getToken('accessToken')) {
          //   this.decodeToken = JWT(this.getToken('accessToken'));
          // }
          this.setToken('userId', success.data._id)
          this.setToken('accessToken', success.data.accessToken);
          this.setToken('fullname', success.data.fullname)
          if (success.data.role == 'user') {
            if (success.data.is_profile_complete == false) {
              this.router.navigate(["/main/update-profile"]);
            }
            else {
              this.router.navigate(["/main/dashboard"]);
            }
          }
          else {
            if (this.decodeToken.data.is_profile_complete == false) {
              this.router.navigate(["/main/attorney/attorney-update-profile"]);
            }
            else {
              this.router.navigate(["/main/attorney/attorney-dashboard"]);
            }
          }
        }
        else {
          setTimeout(() => {
            this.spinner.hide();
          }, 1000);
          if (!this.errorMessageStatus) {
            this.errorMessageStatus1 = success.message;
            this.errorMessageStatus = true;
            setTimeout(() => {
              this.errorMessageStatus = false;
            }, 1000);
          }
        }
      })
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);
    }
  }
  // ***********************************************************************//
}
