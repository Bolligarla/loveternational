import { Component, OnInit, Injector, ElementRef, ViewChild, PLATFORM_ID, Inject } from '@angular/core';
import { BaseComponent } from '../../common/commonComponent';
import { CountryCode } from '../../common/countrycodeList'
import { id } from '@swimlane/ngx-charts/release/utils';
import * as RecordRTC from 'recordrtc';
import { from } from 'rxjs';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
declare var videojs: any;
import * as adapter from 'webrtc-adapter/out/adapter_no_global.js';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.css']
})
export class UpdateProfileComponent extends BaseComponent implements OnInit {
  public user: any = {};
  public countryArr: Array<any> = [];
  public nextData;
  public image: any;
  public OptionFlag: boolean = false;
  public FirstOptionFlag: boolean = false;
  public FirstRemoveFlag: boolean = false;
  public VideoOpenOptionFlag: boolean = false;
  public file: any;
  public image1: any;
  public userId: any;
  public currentImageIndex: any;
  public token: any;
  public profile_pic_1: string;
  public profile_pic_2: string;
  public profile_pic_3: string;
  public profile_pic_4: string;
  public profile_pic_5: string;
  public profile_pic_6: string;
  public url1: any = {};
  public tempArray: any = [];
  public videofile: File;
  public videoPreview: any;
  public role: any;
  public userNameError: boolean = false;
  public userNameMessage: any;
  public imgMessage: any;
  public imgError: boolean = false;
  public genderMessage: any;
  public genderError: boolean = false;
  public interestedMessage: any;
  public interestedError: boolean = false;
  public birthdayMessage: any;
  public birthdayError: boolean = false;
  public liveinMessage: any;
  public liveinError: boolean = false;
  public citizenMessage: any;
  public citizenError: boolean = false;
  public aboutMessage: any;
  public aboutError: boolean = false;
  public birthdayVal: any;
  public dataVisible: boolean = false;
  public currentDate: Date = new Date();
  /************************************
   6 upload static images
   ***********************************/
  public SecondImageArray: Array<any> = [
    { profile2: 'assets/images/no-image-user.png' },
    { profile2: 'assets/images/no-image-user.png' },
    { profile2: 'assets/images/no-image-user.png' },
    { profile2: 'assets/images/no-image-user.png' },
    { profile2: 'assets/images/no-image-user.png' },
    { profile2: 'assets/images/no-image-user.png' },
  ]

  /************************************
    Gender dropdown
    ***********************************/
  public gender = [
    { name: 'Select Gender', value: 'Select Gender' },
    { name: 'Male', value: 'Male' },
    { name: 'Female', value: 'Female' },
    { name: 'Other', value: 'Other' },
  ]
  constructor(inj: Injector, @Inject(PLATFORM_ID) platformId: Object) {
    super(inj);
    this.platformId = platformId;
  }

  ngAfterViewInit() {
  }


  ngOnInit() {
    // this.role = this.data.user_type;
    // this.userId = this.decodeToken.data._id;
    // this.getCountryList();
    this.countryArr = CountryCode();
    console.log(this.countryArr, "countryArr");

    this.user.want_child = false;
    this.user.i_smoke = false;
  }

  /***************************************
    First profile Drop Down function
  *****************************************/
  OpenOption() {
    this.OptionFlag = true;
    this.FirstOptionFlag = false;
  }
  CloseOption() {
    this.OptionFlag = false;
    this.FirstOptionFlag = false;
    $('body').removeClass('open-option');
  }
  /***************************************
    First profile Open File event
  *****************************************/
  openfile(event: any) {
    event.preventDefault();
    let element: HTMLElement = document.getElementById('profile') as HTMLElement;
    element.click();
  }

  /***************************************
    6 profile Drop Down functions
  *****************************************/
  FirstOpenOption(index) {
    this.FirstOptionFlag = true;
    this.currentImageIndex = index;
  }
  FirstCloseOption() {
    this.FirstOptionFlag = false;

  }

  /***************************************
      video dropdown
  *****************************************/
  VideoOpenOption() {
    this.VideoOpenOptionFlag = true;
  }

  VideoCloseOption() {
    this.VideoOpenOptionFlag = false;
  }
  /***************************************
    6 profile Ptofile Open function
  *****************************************/
  FirstOpenFile(event, type?) {
    this.VideoOpenOptionFlag = false;
    event.preventDefault();
    let element = document.getElementById((type ? 'profile3' : 'profile2') + this.currentImageIndex).click();

  }


  /***************************************
   All images uploading from Gallery
  *****************************************/
  fileChangeEvent(event: any, profile, index, file): void {
    this.VideoOpenOptionFlag = false;
    if (profile == 'profile') {
      this.file = event.target.files[0];
      // this.image = event.target.files[0];
      // this.image = URL.createObjectURL(this.image);
      // event.preventDefault();
      this.imageUpload(this.file)

    }

    if (profile == 'profile2') {
      this.tempArray[index] = event.target.files[0];
      for (var i = 0; i < this.tempArray.length; i++) {
        this.profile_pic_1 = this.tempArray[0];
        this.profile_pic_2 = this.tempArray[1];
        this.profile_pic_3 = this.tempArray[2];
        this.profile_pic_4 = this.tempArray[3];
        this.profile_pic_5 = this.tempArray[4];
        this.profile_pic_6 = this.tempArray[5];
      }
      this.url1 = event.target.files[0];
      this.url1["profile2"] = URL.createObjectURL(this.url1);
      this.SecondImageArray[this.currentImageIndex] = this.url1;
      if (this.tempArray.length) {
        var fd1 = new FormData();
        fd1.append('profile_pic_1', this.profile_pic_1 ? this.profile_pic_1 : " ");
        fd1.append('profile_pic_2', this.profile_pic_2 ? this.profile_pic_2 : " ");
        fd1.append('profile_pic_3', this.profile_pic_3 ? this.profile_pic_3 : " ");
        fd1.append('profile_pic_4', this.profile_pic_4 ? this.profile_pic_4 : " ");
        fd1.append('profile_pic_5', this.profile_pic_5 ? this.profile_pic_5 : " ");
        fd1.append('profile_pic_6', this.profile_pic_6 ? this.profile_pic_6 : " ");
        this.commonService.callApi('addUpdateImages/' + this.userId, fd1, 'post', true, false).then(success => {
          if (success.settings.status == 1) {
          }
          else {
            this.popToast('error', success.settings.message)
          }
        })
      }
    }
    else if (profile == 'profile3') {
      this.videofile = event.target.files[0];
      if (this.videofile) {
        let file = new FormData();
        file.append('video_url', this.videofile);
        this.spinner.show();
        this.commonService.callApi("uploadProfileVideo/" + this.userId, file, 'post', true, true).then(success => {
          if (success.settings.status == 1) {
            this.videoPreview = success.data.video_url_thumb;
            this.spinner.hide();

          }
        })
      }
    }
    this.OptionFlag = false;
    this.FirstOptionFlag = false;
  }

  imageUpload(file) {
    console.log(file, "file");
    var fd = new FormData();
    fd.append('file', file);

    this.commonService.callApi("fileUpload", fd, 'post', true, false).then(success => {
      if (success.status == 1) {
        this.image = success.data.filePath;
        console.log(this.image, "this.image ");


        this.spinner.hide();

      }
    })
  }




  /*********************************************
            Error Message Before Next button
    *****************************************/

  over(about_me, username, image, gender, interested_in, live_in, citizen_of, birthday) {
    if ((gender == undefined) || (interested_in == undefined) || (live_in == undefined) || (citizen_of == undefined) || (birthday == undefined)) {
      this.user.gender = undefined;
      this.user.interested_in = undefined;
      this.user.live_in = undefined;
      this.user.citizen_of = undefined;
      this.user.birthday = undefined;
    }

    if (image == undefined || !image) {
      this.imgMessage = 'Please upload your profile picture'
      this.imgError = true;
      setTimeout(() => {
        this.imgError = false;
      }, 1000);
    }
    else if (username == undefined || !username) {
      this.userNameMessage = 'Plese enter your user name'
      this.userNameError = true;
      setTimeout(() => {
        this.userNameError = false;
      }, 1000);
    }
    else if (about_me == undefined || !about_me) {
      this.aboutMessage = 'Plese enter some desciption about you'
      this.aboutError = true;
      setTimeout(() => {
        this.aboutError = false;
      }, 1000);
    }
    if (about_me && username && image) {
      this.nextData = true;
    }
  }
  back(user) {
    this.nextData = false;

  }

  changeStatus(event, key) {
    this.user[key] = !event;
  }

  /*********************************************
               Date Validation Message 
  *****************************************/
  onValueChange(event) {
    if (event) {
      let x = this.currentDate.getFullYear() - event.getFullYear();
      if (x < 18) {
        this.user.birthday = '';
        this.birthdayMessage = 'Only 18 years above can select the birth date'
        this.birthdayError = true;
        setTimeout(() => {
          this.birthdayError = false;
        }, 1000);
      }
    }
  }
  /*********************************************
               submmiting Form
  ********************************************/
  submitUserForm(form, user) {
    console.log(user, "users dat");
    if (user.gender == undefined) {
      this.genderMessage = 'Please select Gender'
      this.genderError = true;
      setTimeout(() => {
        this.genderError = false;
      }, 1000);
    }
    else if (user.interested_in == undefined) {
      this.interestedMessage = 'Please select your interest'
      this.interestedError = true;
      setTimeout(() => {
        this.interestedError = false;
      }, 1000);
    }
    else if (form.form.controls.birthday.value == undefined) {
      this.birthdayMessage = 'Please select Birthday'
      this.birthdayError = true;
      setTimeout(() => {
        this.birthdayError = false;
      }, 1000);
    }
    // else if (!(form.form.controls.live_in.valid)) {
    //   this.liveinMessage = 'Please select Live_In'
    //   this.liveinError = true;
    //   setTimeout(() => {
    //     this.liveinError = false;
    //   }, 1000);
    // }
    // else if (user.citizen_of == undefined) {
    //   this.citizenMessage = 'Please select Citizen'
    //   this.citizenError = true;
    //   setTimeout(() => {
    //     this.citizenError = false;
    //   }, 1000);
    // }
    // else if (form.valid) {
    // let fd = new FormData();
    // fd.append('username', user.username);
    // fd.append('profile_url', this.file);
    // fd.append('about_me', user.about_me);
    // fd.append('birthday', user.birthday);
    // fd.append('live_in', user.live_in);
    // fd.append('citizen_of', user.citizen_of);
    // fd.append('gender', user.gender);
    // fd.append('interested_in', user.interested_in);
    // fd.append('want_child', user.want_child);
    // fd.append('i_smoke', user.i_smoke);
    user.userId = this.getToken('userId');
    user.image = this.image;
    this.spinner.show();
    this.commonService.callApi('/addUpdateProfile', user, 'post', true, true).then(success => {
      if (success.status == 1) {
        setTimeout(() => {
          this.spinner.hide()
        });
        this.setToken('profile_url', success.data.image);
        this.router.navigate(["/main/dashboard"]);
      }
      else {
        this.birthdayVal = success.settings.message;
        setTimeout(() => {
          this.birthdayVal = "";
          this.spinner.hide()
        }, 1000);
      }

    })
    // }
  }
  /*********************************************
             Api For Country Drop Down
  ************************************************/

  getCountryList() {
    this.commonService.callApi('getCountryList/user', '', 'get', false, true).then(success => {
      if (success.settings.status == 1) {
        this.countryArr = success.data;
      } else {
        this.popToast('error', success.message)
      }
    })
  }

}






