import { Component, OnInit, Injector, TemplateRef } from '@angular/core';
import { BaseComponent } from '../../common/commonComponent';
import { AuthService } from 'angular-6-social-login';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
declare var $: any;
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent extends BaseComponent implements OnInit {
  public usertype: boolean = false;
  public show: boolean = false;
  public passtype: string = "password";
  public singUp: boolean = false;
  public signup: any = {};
  public role: any;
  public option: any;
  public submitted: boolean = false;
  public submitted1: boolean = false;
  public submitted3: boolean = false;
  public activefb: boolean = true;
  public errorMessageStatus: boolean = false;
  public errorMessageStatus1: any;
  public modalRef: BsModalRef;
  public deviceTypeId: Number;
  public pswLength;
  public passwordMsg;
  constructor(private socialAuthService: AuthService, inj: Injector, private modalService: BsModalService) {
    super(inj);
  }
  ngOnInit() {
    if (!navigator.mediaDevices || !navigator.mediaDevices.enumerateDevices) {
      return;
    }
    navigator.mediaDevices.enumerateDevices()
      .then(function (devices) {
        devices.forEach((device) => {
          this.deviceTypeId = device.deviceId;
        });
      })
      .catch(function (err) {
      });
  }
  /***********************************
      select user or attorney
   ***********************************/
  optionSelected(event) {
    this.option = event;
  }
  over(event) {
    if (event == undefined) {
      this.usertype = true;
      setTimeout(() => {
        this.usertype = false;
      }, 1000);
    }
    if (this.option) {
      this.role = this.option;
      if (this.role == 'user') {
        this.activefb = false;
      }
      if (this.role == 'attorney') {
        this.activefb = true;
      }
      this.singUp = true;
    }
  }
  /*****************************************
    Back to user selection role
   *****************************************/
  back() {
    this.singUp = false;
    this.usertype = false;
  }
  /**************** open modal for terms and conditions ***********/
  OpenFilter() {
    $('body').addClass('open-filter');
  }

  /**************** close modal for terms and conditions ***********/
  CloseFilter() {
    $('body').removeClass('open-filter');
  }

  openModal(template: TemplateRef<any>, i) {
    this.modalRef = this.modalService.show(template);
  }
  /***************************************
    password type text or password
  ***************************************/
  showpass() {
    this.show = !this.show;
    if (this.show) {
      this.passtype = "text";
    }
    else {
      this.passtype = "password";
    }
  }
  /**********************************************
         API call for user  registration
  ***********************************************/
  signUp(form, signup) {
    if (signup.emailId) {
      signup.email = signup.email.toLowerCase()
    }
    else if (!form.form.controls.full_name.valid) {
      this.submitted = true;
      this.errorMessageStatus = false;
      setTimeout(() => {
        this.submitted = false;
      }, 1000);
    }
    else if (!form.form.controls.email.valid) {
      this.submitted1 = true;
      this.errorMessageStatus = false;
      setTimeout(() => {
        this.submitted1 = false;
      }, 1000);
    }
    else if (signup.password == undefined) {
      this.passwordMsg = "Please enter your password";
      setTimeout(() => {
        this.passwordMsg = ""
      }, 1000);
    }
    // else if (signup.password.length < 8) {
    //   this.pswLength = "The password must be of minimum length 8 characters";
    //   setTimeout(() => {
    //     this.pswLength = ""
    //   }, 1000);
    // }
    else {
      this.submitted3 = true;
      this.errorMessageStatus = false;
      setTimeout(() => {
        this.submitted3 = false;
      }, 1000);
    }
    let data = {
      full_name: signup.full_name,
      email: signup.email,
      password: signup.password,
      user_type: this.option,
    }
    if (form.valid) {
      this.spinner.show();
      this.commonService.callApi('userRegister', data, 'post', false, true).then(success => {
        if (success.status == 1) {
          this.spinner.hide();
          this.router.navigate(["/sign-in"]);
          this.swal({
            title: 'Loveternational',
            text: success.message,
            confirmButtonText: 'Ok',
            allowOutsideClick: false,
            buttonsStyling: false
          })
        }
        else {
          this.errorMessageStatus = true;
          this.errorMessageStatus1 = success.settings.message;
          setTimeout(() => {
            this.spinner.hide();
            this.errorMessageStatus = false;
          }, 1000);
        }
      })
    }
    else {
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);
    }
  }
}
