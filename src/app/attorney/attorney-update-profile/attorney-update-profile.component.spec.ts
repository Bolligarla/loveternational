import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttorneyUpdateProfileComponent } from './attorney-update-profile.component';

describe('AttorneyUpdateProfileComponent', () => {
  let component: AttorneyUpdateProfileComponent;
  let fixture: ComponentFixture<AttorneyUpdateProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttorneyUpdateProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttorneyUpdateProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
