import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttorneySettingComponent } from './attorney-setting.component';

describe('AttorneySettingComponent', () => {
  let component: AttorneySettingComponent;
  let fixture: ComponentFixture<AttorneySettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttorneySettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttorneySettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
