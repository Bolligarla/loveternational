import { Component, OnInit, Injector, ViewChild, ElementRef } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent'
import { OwlOptions } from 'ngx-owl-carousel-o';
import { log } from 'util';

declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-attorney-profile',
  templateUrl: './attorney-profile.component.html',
  styleUrls: ['./attorney-profile.component.css']
})
export class AttorneyProfileComponent extends BaseComponent implements OnInit {
  public ratingReview: any = {};
  public totalcount: Array<any> = [];
  public Edit: boolean = false;
  public id: any;
  public attorney: any = {};
  public attorneyArr: any = {};
  public profileUrl: any;
  public licenseUrl: any;
  public imagevalid: boolean = false;
  public countryArr: Array<any> = [];
  public stateArr: Array<any> = [];
  public full_name: any;
  public ratings: boolean = false;
  public role: any;
  public year = [{ name: "1 Year", value: 1 }, { name: "2 Year", value: 2 }, { name: "3 Year", value: 3 }, { name: "4 Year", value: 4 }, { name: "5 Year", value: 5 }, { name: "6 Year", value: 6 }, { name: "7 Year", value: 7 }, { name: "8 Year", value: 8 }, { name: "9 Year", value: 9 }, { name: "10 Year", value: 10 },
  { name: "11 Year", value: 11 }, { name: "12 Year", value: 12 }, { name: "13 Year", value: 13 }, { name: "14 Year", value: 14 }, { name: "15 Year", value: 15 }, { name: "16 Year", value: 16 }, { name: "17 Year", value: 17 }, { name: "18 Year", value: 18 }, { name: "19 Year", value: 19 }, { name: "20 Year", value: 20 },
  { name: "21 Year", value: 21 }, { name: "22 Year", value: 22 }, { name: "23 Year", value: 23 }, { name: "24 Year", value: 24 }, { name: "25 Year", value: 25 }, { name: "26 Year", value: 26 }, { name: "27 Year", value: 27 }, { name: "28 Year", value: 28 }, { name: "29 Year", value: 29 }, { name: "30 Year", value: 30 },
  { name: "31 Year", value: 31 }, { name: "32 Year", value: 32 }, { name: "33 Year", value: 33 }, { name: "34 Year", value: 34 }, { name: "35 Year", value: 35 }, { name: "36 Year", value: 36 }, { name: "37 Year", value: 37 }, { name: "38 Year", value: 38 }, { name: "39 Year", value: 39 }, { name: "40 Year", value: 40 },
  { name: "41 Year", value: 41 }, { name: "42 Year", value: 42 }, { name: "43 Year", value: 43 }, { name: "44 Year", value: 44 }, { name: "45 Year", value: 45 }, { name: "46 Year", value: 46 }, { name: "47 Year", value: 47 }, { name: "48 Year", value: 48 }, { name: "49 Year", value: 49 }, { name: "50 Year", value: 50 }
  ]
  customOptions: OwlOptions = {
    loop: true,
    dots: false,
    autoWidth: true,
    nav: false,
  }
  constructor(inj: Injector) {
    super(inj)
  }

  ngOnInit() {
    this.role = this.decodeToken.data.user_type;
    this.id = this.decodeToken.data._id;
    this.full_name = this.decodeToken.data.full_name
    this.getAttorneyProfile();
    this.getAttorneyRatingView();
  }
  OpenMenu() {
    $('body').addClass('open-menu');
  }
  back() {
    this.Edit = false;
  }
  edit() {
    this.Edit = true;
    this.getCountryList();
  }
  public experience: any = {};
  public reviewArr: any = {};
  getAttorneyProfile() {
    this.commonService.callApi('getUserProfile/' + this.id, '', 'get', true, true).then(success => {
      if (success.settings.status == 1) {
        this.attorneyArr = success.data;
        this.reviewArr = success.review;
        this.image = success.data.profile_url;
        this.profileUrl1 = success.data.license_photo;
        this.attorney = success.data.country_id;
        this.imagevalid = false;
      }
    })
  }

  getCountryList() {
    this.commonService.callApi('getCountryList/attroney', '', 'get', false, true).then(success => {
      if (success.settings.status == 1) {
        this.countryArr = success.data;
        this.getStateList();
      }
    })
  }

  changeCountry(event) {
    this.attorney = this.attorneyArr.country_id;
    this.getStateList();
  }

  getStateList() {
    this.commonService.callApi('getStateListByCountryId/' + this.attorney, '', 'get', false, true).then(success => {
      if (success.settings.status == 1) {
        this.stateArr = success.data;
        this.attorneyArr.state_id = this.stateArr[0]._id;
      }
    })
  }
  public submitted: boolean = false;
  public imgMessage: any;
  public imgError: boolean = false;
  public firmMessage: any;
  public firmError: boolean = false;
  public profileMessage: any;
  public profileError: boolean = false;
  public licenseMessage: any;
  public licenseError: boolean = false;
  public aboutMessage: any;
  public aboutError: boolean = false;
  public nameMessage: any;
  public nameError: boolean = false;
  public errorMessageStatus: boolean = false;
  attorneyUpdateProfile(form, attorneyArr) {
    if (this.image == undefined) {
      this.imgMessage = 'Please upload your profile picture'
      this.imgError = true;
      setTimeout(() => {
        this.imgError = false;
      }, 1000);
    }
    else if (this.profileUrl1 == undefined) {
      this.profileMessage = 'Please upload your license picture'
      this.profileError = true;
      setTimeout(() => {
        this.profileError = false;
      }, 1000);
    }
    else if (!form.form.controls.full_name.valid) {
      this.nameMessage = 'Please enter your firmname'
      this.nameError = true;
      setTimeout(() => {
        this.nameError = false;
      }, 1000);
    }
    else if (!form.form.controls.name_of_firm.valid) {
      this.firmMessage = 'Please enter your firmname'
      this.firmError = true;
      setTimeout(() => {
        this.firmError = false;
      }, 1000);
    }
    else if (!form.form.controls.license_no.valid) {
      this.licenseMessage = 'Please enter your license number'
      this.licenseError = true;
      setTimeout(() => {
        this.licenseError = false;
      }, 1000);
    }
    else if (!form.form.controls.paypal_email_address.valid) {
      this.submitted = true;
      this.errorMessageStatus = false;
      setTimeout(() => {
        this.submitted = false;
      }, 1000);
    }
    else if (!form.form.controls.about_me.valid) {
      this.aboutMessage = 'Please enter some description about your service'
      this.aboutError = true;
      setTimeout(() => {
        this.aboutError = false;
      }, 1000);
    }
    else if (form.valid) {
      var fd = new FormData();
      if (this.file) {
        fd.append('profile_url', this.file);
      }
      else {
        fd.append('profile_url', this.image);
      }
      fd.append('full_name', this.attorneyArr.full_name);
      fd.append('name_of_firm', this.attorneyArr.name_of_firm);
      fd.append('country_id', this.attorneyArr.country_id);
      fd.append('state_id', this.attorneyArr.state_id);
      fd.append('experience', this.attorneyArr.experience);
      if (this.file1) {
        fd.append('license_photo', this.file1);
      }
      else {
        fd.append('license_photo', this.profileUrl1);
      }
      fd.append('license_no', this.attorneyArr.license_no);
      fd.append('about_me', this.attorneyArr.about_me);
      fd.append('paypal_email_address', this.attorneyArr.paypal_email_address);
      this.spinner.show()
      this.commonService.callApi('addEditProfile/' + this.id, fd, 'post', true, true).then(success => {
        if (success.settings.status == 1) {
          setTimeout(() => {
            this.spinner.hide()
          });
          this.setToken('profile_url', success.data.profile_url);
          this.setToken('full_name', success.data.full_name);
          this.Edit = false;
          this.getAttorneyProfile();
        }
      })
    }
  }
  public file: any;
  public file1: any;
  public image: any;
  public profileUrl1: any;
  public canvasVisible: boolean = true;
  public videoVisible: boolean = true;
  public OptionFlag: boolean = false;
  public FirstOptionFlag: boolean = false;
  public canvasType: any;
  public takePicButton: boolean = false;
  public sliceSize: any;
  public cancel: boolean = true
  public VideoOpenOptionFlag: boolean = false;
  public OptionLicenseFlag: boolean = false;
  public FirstOptionLicenseFlag: boolean = false;
  public licensecanvasType: any;
  public licensecanvasVisible: boolean = true;
  public licensevideoVisible: boolean = true;
  public licensetakePicButton: boolean = false;
  public VideoOpenOptionLicenseFlag: boolean = false;
  public profile: any;
  public licenseprofile: any;
  // @ViewChild("canvas") canvas: ElementRef;
  // @ViewChild("licensecanvas") licensecanvas: ElementRef;
  // @ViewChild("video") video: ElementRef;
  // @ViewChild("licensevideo") licensevideo: ElementRef;

  // takePic(event) {
  //   this.canvasVisible = true;
  //   this.videoVisible = true;
  //   this.OptionFlag = false;
  //   this.FirstOptionFlag = false;
  //   this.licensecanvasVisible = true;
  //   this.licensevideoVisible = true;
  //   this.OptionLicenseFlag = false;
  //   this.FirstOptionLicenseFlag = false;
  //   if (event == 'canvas') {
  //     $('body').removeClass('open-option');
  //     this.canvasType = this.canvas;
  //     this.canvasVisible = false;
  //     var context = this.canvasType.nativeElement.getContext("2d").drawImage(this.video.nativeElement, 0, 0, 400, 300);
  //     this.image = this.canvasType.nativeElement.toDataURL("image/png");
  //     var block = this.image.split(";");
  //     var contentType = block[0].split(":")[1];
  //     var realData = block[1].split(",")[1];
  //     this.b64toBlob(realData, contentType, this.sliceSize, 'canvas');
  //     this.profile = URL.createObjectURL(this.file);
  //     this.videoVisible = false;
  //     this.canvasVisible = false;
  //     this.takePicButton = false;
  //   }
  //   if (event == 'licensecanvas') {
  //     $('body').removeClass('open-option');
  //     this.licensecanvasType = this.licensecanvas;
  //     this.licensecanvasVisible = false;
  //     var context = this.licensecanvasType.nativeElement.getContext("2d").drawImage(this.licensevideo.nativeElement, 0, 0, 400, 300);
  //     this.profileUrl1 = this.licensecanvasType.nativeElement.toDataURL("image/png");
  //     var block = this.profileUrl1.split(";");
  //     var contentType = block[0].split(":")[1];
  //     var realData = block[1].split(",")[1];
  //     this.b64toBlob(realData, contentType, this.sliceSize, 'licensecanvas');
  //     this.licenseprofile = URL.createObjectURL(this.file1);
  //     this.licensevideoVisible = false;
  //     this.licensecanvasVisible = false;
  //     this.licensetakePicButton = false;
  //   }
  //   this.canvasVisible = false;
  //   this.licensecanvasVisible = false;
  // }
  // b64toBlob(b64Data, contentType, sliceSize, option) {
  //   contentType = contentType || '';
  //   sliceSize = sliceSize || 512;
  //   var byteCharacters = atob(b64Data); i
  //   var byteArrays = [];
  //   for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
  //     var slice = byteCharacters.slice(offset, offset + sliceSize);
  //     var byteNumbers = new Array(slice.length);
  //     for (var i = 0; i < slice.length; i++) {
  //       byteNumbers[i] = slice.charCodeAt(i);
  //     }
  //     var byteArray = new Uint8Array(byteNumbers);
  //     byteArrays.push(byteArray);
  //   }
  //   var blob = new Blob(byteArrays, { type: contentType });
  //   if (option == 'canvas') {
  //     this.file = new File([blob], "uploaded_file.jpg", { type: contentType, lastModified: Date.now() });
  //   }
  //   if (option == 'licensecanvas') {
  //     this.file1 = new File([blob], "uploaded_file.jpg", { type: contentType, lastModified: Date.now() });
  //   }
  // }

  // methodCancel() {
  //   this.videoVisible = false;
  //   this.canvasVisible = false;
  //   this.takePicButton = false;
  //   this.licensevideoVisible = false;
  //   this.licensecanvasVisible = false;
  //   this.licensetakePicButton = false
  //   this.cancel = false;
  // }
  OpenOption(data) {
    if (data == 'photo') {
      this.OptionFlag = true;
      this.FirstOptionFlag = false;
    }
    else {
      this.OptionLicenseFlag = true;
      this.FirstOptionLicenseFlag = false;
    }
  }

  fileChangeEvent(event: any, profile): void {
    this.VideoOpenOptionFlag = false;
    this.VideoOpenOptionLicenseFlag = false;
    if (profile == 'profile') {
      this.file = event.target.files[0];
      this.image = URL.createObjectURL(this.file);
      event.preventDefault();
    }
    else {
      this.file1 = event.target.files[0];
      this.profileUrl1 = URL.createObjectURL(this.file1);
      event.preventDefault();
    }
    this.OptionFlag = false;
    this.FirstOptionFlag = false;
    this.OptionLicenseFlag = false;
    this.FirstOptionLicenseFlag = false;
  }
  openfile(event: any, profile) {
    if (profile == 'photo') {
      document.getElementById('photo').click();
    }
    if (profile == 'license') {
      document.getElementById('license').click();
    }
  }
  // changeCamera(event) {
  //   if (event == 'useCamera') {
  //     this.takePicButton = true;
  //     this.videoVisible = true;
  //     this.canvasVisible = true;
  //     $('body').addClass('open-option');
  //     if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
  //       navigator.mediaDevices.getUserMedia({ video: true }).then(stream => {
  //         this.video.nativeElement.srcObject = stream;
  //         this.FirstOptionFlag = false;
  //         this.OptionFlag = false;
  //       });
  //     }
  //   }
  //   if (event == 'useCamera1') {
  //     this.licensetakePicButton = true;
  //     this.licensevideoVisible = true;
  //     this.licensecanvasVisible = true;
  //     $('body').addClass('open-option');
  //     if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
  //       navigator.mediaDevices.getUserMedia({ video: true }).then(stream => {
  //         this.licensevideo.nativeElement.srcObject = stream;
  //         this.FirstOptionLicenseFlag = false;
  //         this.OptionLicenseFlag = false;
  //       });
  //     }
  //   }
  //   this.FirstOptionFlag = false;
  //   this.OptionFlag = false;
  //   this.FirstOptionLicenseFlag = false;
  //   this.OptionLicenseFlag = false;
  // }
  CloseOption() {
    this.OptionFlag = false;
    this.FirstOptionFlag = false;
    this.FirstOptionLicenseFlag = false;
    this.OptionLicenseFlag = false;
    $('body').removeClass('open-option');
  }

  getAttorneyRatingView() {
    let data = {
      page: "1",
      limit: "10",
      id: this.id
    }
    this.commonService.callApi('getAttorneyRatings', data, 'post', true, true).then((success) => {
      if (success.settings.status == 1) {
        this.ratingReview = success.data.data;
        this.totalcount = success.data;
        if (success.data.list.length == 0) {
          this.ratings = true;
        }
        else {
          this.ratings = false;
        }
      }
    })
  }
}
