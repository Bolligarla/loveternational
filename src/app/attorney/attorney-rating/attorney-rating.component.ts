import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';
@Component({
  selector: 'app-attorney-rating',
  templateUrl: './attorney-rating.component.html',
  styleUrls: ['./attorney-rating.component.css']
})
export class AttorneyRatingComponent extends BaseComponent implements OnInit {

  public role: any;
  public attorney_id: any;
  public ratingReview: any = {};
  public ratingUsers: Array<any> = [];

  constructor(inj: Injector) {
    super(inj);
    this.activatedRoute.params.subscribe(params => {
      if (params['id']) {
        this.attorney_id = params['id'];
      }
    })
  }

  ngOnInit() {
    this.role = this.decodeToken.data.user_type;
    this.getAttorneyRatingView();
  }

  /**********************************************
     back to attorney-profile or attorney-list
   *********************************************/
  back() {
    if (this.role == 'attorney') {
      this.router.navigate(['/main/attorney/attorney-profile']);
    }
    else {
      this.router.navigate(['/main/attorney-list']);
    }
  }

  /*****************************************
      get Attorney ratings for view
  ****************************************/
  getAttorneyRatingView() {
    let data = {
      page: "1",
      limit: "10",
      id: this.attorney_id
    }
    this.commonService.callApi('getAttorneyRatings', data, 'post', true, true).then((success) => {
      if (success.settings.status == 1) {
        this.ratingReview = success.data.data;
        this.ratingUsers = success.data.list;
      }
    })
  }
}
