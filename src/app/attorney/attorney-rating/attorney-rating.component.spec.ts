import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttorneyRatingComponent } from './attorney-rating.component';

describe('AttorneyRatingComponent', () => {
  let component: AttorneyRatingComponent;
  let fixture: ComponentFixture<AttorneyRatingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttorneyRatingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttorneyRatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
