import { Component, OnInit, Injector, ViewChild, ElementRef, HostListener } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent'
import { isPlatformBrowser } from '@angular/common';
declare var $: any;

@Component({
  selector: 'app-attorney-dashboard',
  templateUrl: './attorney-dashboard.component.html',
  styleUrls: ['./attorney-dashboard.component.css']
})
export class AttorneyDashboardComponent extends BaseComponent implements OnInit {
  @ViewChild('loading') loading: ElementRef;
  public AttorneyClientsArr: Array<any> = [];
  public noDataList: boolean = false;
  public userData: any = {};
  public role: any;
  public showData = false;
  public nextPage: any;
  public total: any;
  public data: any = {};
  public page: any = 1;
  public pageNo;
  public callBackSuccess: boolean = true;
  constructor(inj: Injector) {
    super(inj)
  }

  ngOnInit() {
    this.role = this.decodeToken.data.user_type;
    this.spinner.show();
    setTimeout(() => {
      this.getAttornyClientList();
    }, 1000);
  }
  /********************************
         Open menu bar
   *********************************/
  OpenMenu() {
    $('body').addClass('open-menu');
  }

  /********************************
         get the client's list
   *********************************/
  getAttornyClientList() {
    this.data = {
      page: this.page.toString(),
      limit: "10",
    }
    this.callBackSuccess = false;
    this.commonService.callApi('getAttornyClientList', this.data, 'post', true, true).then((success) => {
      if (success.settings.status == 1) {
        this.AttorneyClientsArr = success.data.list;
        this.total = success.data.totalRecord;
        this.nextPage = success.data.nextPage;
        this.pageNo = success.data.nextPage;
        if (success.data.list == "") {
          setTimeout(() => {
            this.spinner.hide();
            this.noDataList = true;
          }, 1000);
        }
        else {
          setTimeout(() => {
            this.spinner.hide();
            this.noDataList = false;
          }, 1000);
        }
      }
      else
      {
        setTimeout(() => {
          this.spinner.hide();
          this.noDataList = true;
        }, 1000);
      }
      this.callBackSuccess = true;
    },
      error => {
        this.callBackSuccess = true;
        this.spinner.hide();
      })
  }

  /********************************************
       attorney Listing for scrolling
   *******************************************/
  onScrollDown() {
    this.callBackSuccess = false;
    if (this.data.page != undefined) {
      this.data.page = (JSON.parse(this.data.page) + 1).toString();
    }

    if (this.pageNo) {
      this.commonService.callApi('getAttornyClientList', this.data, 'post', true, true).then((success) => {
        if (success.settings.status == 1) {
          this.AttorneyClientsArr = success.data.list;
          this.total = success.data.totalRecord;
          this.nextPage = success.data.nextPage;
          this.callBackSuccess = true;
        }
        error => {
          this.spinner.hide();
          this.callBackSuccess = true;
        }
      })
    }
    else {
      this.spinner.hide();
    }
  }

  /********************************************
     attorney Listing for scrolling
  *******************************************/
  @HostListener('window:scroll', ["$event"])
  onWindowScroll() {
    if (isPlatformBrowser(this.platformId)) {
      const componentPosition = this.loading.nativeElement.offsetTop - 800;
      const scrollPosition = window.pageYOffset;
      if (scrollPosition >= componentPosition) {
        if (this.nextPage !== 0 && this.callBackSuccess) {
          this.callBackSuccess = false;
          this.onScrollDown();
        }
      }
    }
  }

  /*************** no data available **************/
  ClientsList() {
    this.spinner.show();
    this.noDataList = false;
    this.getAttornyClientList();
  }

  /*****************************************
      Open User Details
   *******************************************/
  openUserDetails(i, user) {
    this.showData = true;
    if (user == 'user') {
      this.userData = i.user
    }
    else {
      this.userData = i.crush
    }
  }

  /************************************************
       Hide Friend list of users details page
   ************************************************/
  hideFriendList() {
    this.showData = false;
  }

  /***********************************************
      Redirect to chat module
   ************************************************/
  redirectToChat(attorneyData) {
    if (this.decodeToken.data.acc_type == "premium") {
      let groupDialog = {
        dialogId: attorneyData.grp_dialg_id,
        selectedQbId: attorneyData.crush.quickbloxId,
        selectedUserName: attorneyData.crush.full_name, attorney_qb_id: attorneyData.user.quickbloxId
      }
      this.setToken('groupDialog', JSON.stringify(groupDialog));
      this.router.navigate(['/main/chat', 'attorney']);
    }
    else {

    }
  }
}
