import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from '../common/commonComponent'
declare var jQuery:any;
declare var $:any; 

@Component({
  selector: 'app-attorney',
  template: `<router-outlet></router-outlet>`,
  styleUrls: ['./attorney.component.css']
})
export class AttorneyComponent extends BaseComponent implements OnInit {

  
  constructor(inj: Injector) {
    super(inj)
  } 

  ngOnInit() {
  }
  
}
