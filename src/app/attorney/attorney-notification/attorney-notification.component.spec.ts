import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttorneyNotificationComponent } from './attorney-notification.component';

describe('AttorneyNotificationComponent', () => {
  let component: AttorneyNotificationComponent;
  let fixture: ComponentFixture<AttorneyNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttorneyNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttorneyNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
