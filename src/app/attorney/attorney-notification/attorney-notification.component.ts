import { Component, OnInit, Injector, ViewChild, ElementRef, HostListener } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent'
import { isPlatformBrowser } from '@angular/common';

declare var jQuery:any;
declare var $:any; 

@Component({
  selector: 'app-attorney-notification',
  templateUrl: './attorney-notification.component.html',
  styleUrls: ['./attorney-notification.component.css']
})
export class AttorneyNotificationComponent extends BaseComponent implements OnInit {
  
@ViewChild('loading') loading:  ElementRef;
  
public notificationArr: Array<any> = [];
public noDataList: boolean = false;
public role: any;
public page: any = 1;
public nextPage: any;
public data: any = {};
public total: any;
public pageNo;
constructor(inj: Injector) {
  super(inj)
}
ngOnInit() {
  this.role = this.decodeToken.data.user_type;
  this.spinner.show();
  setTimeout(() => {
    this.getNotificationsList();
    this.spinner.hide()
  }, 1000);
}
/***************************
      Open menu bar
*****************************/
OpenMenu() {
  $('body').addClass('open-menu');
}

/***************************
  Notifications List
*****************************/
getNotificationsList() {
  this.data = {
    page: this.page.toString(),
    limit: "10",
  }
  this.commonService.callApi('getNotificationLists', this.data, 'post', true, true).then((success) => {
    if (success.settings.status == 1) {
      this.notificationArr = success.data.list;
      this.total = success.data.totalRecord;
      this.nextPage = success.data.nextPage;
      this.pageNo = success.data.nextPage;
      this.notificationArr.forEach(x=>{
        let sample=new Date(x.createdAt)
        x.agoTime=this.moment(sample, "YYYYMMDD").fromNow();
      });
      if (success.data.list == "") {
        setTimeout(() => {
          this.spinner.hide();
          this.noDataList = true;
        }, 1000);
      }
      else {
        setTimeout(() => {
          this.spinner.hide();
          this.noDataList = false;
        }, 1000);
      }
      if (this.nextPage !== 0) {
        this.onWindowScroll();
      }
    }
  })
}
/***************************
  No data in notifications
*****************************/
getNoDataList() {
  this.spinner.show();
  this.noDataList = false;
  this.getNotificationsList();
}


onScrollDown() {
  if (this.data.page  !=  undefined) {
      this.data.page  = (JSON.parse(this.data.page) +  1).toString();
  }
  if (this.pageNo  !==  0) {
      this.commonService.callApi('getNotificationLists' , this.data, 'post', true, true).then(success  => {
          if (success.settings.status  ==  1) {
              if (this.notificationArr.length  <  success.data.totalRecord) {
                  this.pageNo  =  success.data.nextPage
                  this.spinner.hide();
                  this.notificationArr  =  this.notificationArr.concat(success.data.list);
           this.notificationArr.forEach(x=>{
            let sample=new Date(x.createdAt)
               x.agoTime=this.moment(sample, "YYYYMMDD").fromNow();
            });
          }
         if (this.pageNo!== 0) {
            this.onWindowScroll();
          }
          }
      })
  } else {
      this.spinner.hide();
  }
}
/*****************************************
   Users Listing for scrolling
****************************************/
 @HostListener('window:scroll', ["$event"])
 
 onWindowScroll() {
    if (isPlatformBrowser(this.platformId)) {
       const  componentPosition  =  this.loading.nativeElement.offsetTop  -  800;
       const  scrollPosition  =  window.pageYOffset;
         if (scrollPosition  >=  componentPosition) {
            if (this.nextPage  !==  0) {
               this.spinner.show()
               this.onScrollDown();
               }
           }
       }
   }
}
 