import { Component, OnInit, Injector, Input } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent'
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { of, from } from 'rxjs';
import { Observable } from 'rxjs/Observable';
declare var $: any;
@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.css']
})
export class SettingComponent extends BaseComponent implements OnInit {
  public user: any = {};
  public terms: boolean = false;
  public policy: boolean = false;
  public faq: boolean = false;
  public modalRef: BsModalRef;
  public modalRef1: BsModalRef;
  public new_matches: String;
  public new_messages: String;
  public clientToken: any;
  public load: Boolean = false;
  public chargeAmount: any;
  public paymentMessage: any;
  public transctionId: any;
  public role: any;
  public fbId:boolean = false;
  constructor(inj: Injector, private modalService: BsModalService) {
    super(inj);
  }
  ngOnInit() {
    this.role = this.decodeToken.data.user_type;
    if(this.getToken('fbId'))
    {
      this.fbId = true;
    }
    /*********************************************
        Initially called getUserSettings
     *********************************************/
    this.getUserSettings();
  }

  /************************
   Open Terms and conditions
   **************************/
  OpenTerms() {
    $('body').addClass('open-filter');
    this.policy = false;
    this.terms = true;
    this.faq = false;
    if (this.modalRef)
      this.modalRef.hide()
  }
  /************************
   Open Privacy and Policy
   **************************/
  OpenPolicy() {
    $('body').addClass('open-filter');
    this.policy = true;
    this.terms = false;
    this.faq = false;
    if (this.modalRef)
      this.modalRef.hide()
  }
  /************************
      Open FAQ'S
   **************************/
  OpenFaqs() {
    $('body').addClass('open-filter');
    this.policy = false;
    this.terms = false;
    this.faq = true;
    if (this.modalRef)
      this.modalRef.hide()
  }
  /************************
       close filter
  **************************/
  CloseFilter() {
    $('body').removeClass('open-filter');
  }
  /************************
    open menu bar
  **************************/
  OpenMenu() {
    $('body').addClass('open-menu');
  }
  /************************
   Open Modal for Premium
  **************************/
  openModal(template) {
    this.modalRef = this.modalService.show(template);
  }

  /******************************************
   Update notifications matches and messages
  *******************************************/
  notifications(event, key) {
    if (event == true) {
      event = "1"
    } else {
      event = "0"
    }
    if (key == 'new_match') {
      this.new_matches = event;
    }
    if (key == 'new_message') {
      this.new_messages = event;
    }
    let data = {
      new_match: this.new_matches,
      new_message: this.new_messages
    }
    this.commonService.callApi('updateDashBoardSettings', data, 'post').then((success) => {
      if (success.settings.status == 1) {
        this.spinner.show();
        setTimeout(() => {
          this.spinner.hide();
        }, 1000);
      }
    })
  }


  /***********************************
      get user notification settings 
  ************************************/
  getUserSettings() {
    this.commonService.callApi('getUserSettings', '', 'get', true, false).then((success) => {
      if (success.settings.status == 1) {
        this.user = success.data;
        this.chargeAmount = success.data.priceToPay;
      }
    })
  }
  openPremiumModal(template1) {
    this.getClientTokens();
    this.modalRef.hide();
    this.modalRef1 = this.modalService.show(template1);
  }

  /***********************************
       get Client Token
   ************************************/
  getClientTokens() {
    this.commonService.callApi('client_token', '', 'get', true, false).then((success) => {
      if (success.settings.status == 1) {
        this.load = true;
        this.clientToken = success.data.token;
      }
    })
  }
  /***********************************
     returning the client token 
  ************************************/
  getClientTokenFunction(): Observable<any> {
    return of(this.clientToken);
  }
  /***********************************
      creating purchase
   ************************************/
  createPurchase(nonce: string, chargeAmount: number) {
    const observable = from(this.commonService.callApi('checkout', { nonce: nonce, amount: chargeAmount }, 'post', false, false));
    observable.subscribe(success => {
      if (success.settings.status == 1) {
        this.paymentMessage = success.settings.message;
        this.transctionId = this.setToken("transactionId", success.data.txnId)
        setTimeout(() => {
          this.paymentMessage = ''
        }, 1000);
        this.modalRef1.hide();
        this.getUserSettings();
      }
      else{
        this.modalRef1.hide();
        this.popToast('error',success.settings.message);
      }
    })
    return observable;
  }
}
