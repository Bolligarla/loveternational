import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAttorneyRatingComponent } from './user-attorney-rating.component';

describe('UserAttorneyRatingComponent', () => {
  let component: UserAttorneyRatingComponent;
  let fixture: ComponentFixture<UserAttorneyRatingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAttorneyRatingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAttorneyRatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
