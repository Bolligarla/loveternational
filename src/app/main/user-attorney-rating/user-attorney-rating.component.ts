import { Component, OnInit, Injector, TemplateRef } from '@angular/core';
import { BaseComponent } from '../../common/commonComponent';

@Component({
  selector: 'app-user-attorney-rating',
  templateUrl: './user-attorney-rating.component.html',
  styleUrls: ['./user-attorney-rating.component.css']
})
export class UserAttorneyRatingComponent extends BaseComponent implements OnInit {

  public Rate: boolean = false;
  public rateVisible: boolean = false;
  public showVisible: boolean = false;
  public attorney_id: any;
  public data: any;
  public reviewStar: String;
  public ratingUsers: Array<any> = [];
  public ratingReview: any = {};
  public attorneyreview: any = {};
  public errorMessageStatus: boolean = false;
  public errorMessageStatus1: any;

  constructor(inj: Injector) {
    super(inj);
    this.activatedRoute.params.subscribe(params => {
      this.data = params['data'];
      if (this.data == "show") {
        this.Rate = false;
        this.attorney_id = params['id']
      } else {
        this.Rate = true;
        this.rateVisible = true;
        this.showVisible = false;
        this.attorney_id = params['id']
      }
    })
  }

  ngOnInit() {
    this.getAttorneyRatingView();
  }
  back() {
    this.Rate = false
  }

  /***********************************
     back to attorney list
  ************************************/
  back1() {
    this.router.navigate(['/main/attorney-list']);
  }

  /***********************************
    Visible the rate 
  ************************************/
  RateNow() {
    this.Rate = true;
    this.showVisible = true;
    this.rateVisible = false;
  }

  /***********************************
     Viewing the rate value
  ************************************/
  onRate(e) {
    this.reviewStar = e.starRating.value;
  }

  /***********************************
    Updating attorney ratings
  ************************************/
  addEditAttorneyRatings(form, attorneyreview) {
    if (Object.keys(attorneyreview).length) {
      let data = {
        attorney_id: this.attorney_id,
        star: this.reviewStar.toString(),
        review: this.attorneyreview.review
      }
      this.commonService.callApi('addEditAttorneyRatings', data, 'post', true, true).then((success) => {
        if (success.settings.status == 1) {
          this.Rate = false;
          this.getAttorneyRatingView();
        }
      })
    }
    else {
      if (!this.errorMessageStatus) {
        this.errorMessageStatus1 = "Please Pass Parameter : review";
        this.errorMessageStatus = true;
        setTimeout(() => {
          this.errorMessageStatus = false;
        }, 1000);
      }
    }
  }

  /***********************************
       getting the attorney ratings
    ************************************/
  getAttorneyRatingView() {
    let data = {
      page: "1",
      limit: "10",
      id: this.attorney_id
    }
    this.commonService.callApi('getAttorneyRatings', data, 'post', true, true).then((success) => {
      if (success.settings.status == 1) {
        this.ratingReview = success.data.data;
        this.ratingUsers = success.data.list;
      }
    })
  }
}
