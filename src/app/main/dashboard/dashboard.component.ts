import { Component, OnInit, Injector, Input, ViewChild, ElementRef, HostListener, ViewEncapsulation } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent'
import { Options } from 'ng5-slider';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { isPlatformBrowser } from '@angular/common';
import { of, from, Observable } from 'rxjs';
import { OwlOptions } from 'ngx-owl-carousel-o';
declare var $: any;
@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	encapsulation: ViewEncapsulation.None,
	styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent extends BaseComponent implements OnInit {
	@ViewChild('loading') loading: ElementRef;
	public transctionId: any;
	public i_smoke: boolean = false;
	public i_drink: boolean = false;
	public want_child: boolean = false;
	public video_url: any;
	public videoEnabled: boolean = false;
	public videourl: boolean = false;
	public videourlStatus: any;
	public userData: any = {};
	public countryArr: Array<any> = [];
	public noDataList: boolean = false;
	public showData: boolean = false;
	public educationArr: Array<any> = [];
	public discoverListArr: Array<any> = [];
	public discArr: any = {};
	public crushid: any;
	public UserDetailFlag = false;
	public id: any;
	public page: any = 1
	public total: any;
	public filter: boolean = false;
	public modalRef: BsModalRef;
	public modalRef1: BsModalRef;
	public premiumMessage: any;
	public terms: boolean = false;
	public policy: boolean = false;
	public clientToken: any;
	public load: Boolean = false;
	public chargeAmount: any;
	public paymentMessage: any;
	public data: any = {};
	public role: any;
	public nextPage: any;
	public data1: any;
	public pageNo;
	public showProfilePics: Boolean = false;
	public callBackSuccess: boolean = true;
	public options: Options = {
		floor: 18,
		ceil: 80,
		step: 1
	};
	public userObject: any = {};

	constructor(inj: Injector, private modalService: BsModalService) {
		super(inj)
	}

	ngOnInit() {
		// this.role = this.decodeToken.data.user_type;
		this.spinner.show()
		this.discArr.minValue = 18;
		this.discArr.maxValue = 80;
		// this.id = this.decodeToken.data._id;
		// setTimeout(() => {
		// this.getDiscoverList();
		this.getUsersData()
		// }, 1000);
	}
	/************ open menu bar *****************/
	OpenMenu() {
		$('body').addClass('open-menu');
	}
	/************ open filter *****************/
	OpenFilter(modal) {
		$('body').addClass('open-filter');
		if (modal == 'filter') {
			this.getCountryList();
			this.getEducation();
			this.policy = false;
			this.terms = false;
			this.filter = true;
		} else if (modal == 'terms') {
			this.UserDetailFlag = false;
			this.policy = false;
			this.terms = true;
			this.filter = false;
			if (this.modalRef)
				this.modalRef.hide()
		} else {
			this.UserDetailFlag = false;
			this.policy = true;
			this.terms = false;
			this.filter = false;
			if (this.modalRef)
				this.modalRef.hide()
		}
	}
	/********************************************
					 close filter
	  *******************************************/
	CloseFilter() {
		$('body').removeClass('open-filter');
	}

	/********************************
	 open user details page 
	 *******************************/
	UserDetailOpen(i) {
		console.log("enter", i, 123);

		this.UserDetailFlag = true;
		this.videoEnabled = false
		this.userData = i;
		Object.assign(this.userObject, ...this.userData);
	}

	/********************************
	 close user details page 
	*******************************/
	UserDetailClose() {
		this.UserDetailFlag = false;
		this.getDiscoverList();
	}
	/************************
		back to dashboard
	*************************/
	back() {
		this.UserDetailFlag = false;
		this.imageCounter = 0;
		this.userData.profile_url = this.userObject.profile_url;
	}
	back1() {
		this.UserDetailFlag = true;
		this.videoEnabled = false;
	}

	/**************************
	  List of countries
	****************************/
	getCountryList() {
		this.commonService.callApi('getCountryList/user', '', 'get', false, true).then(success => {
			if (success.settings.status == 1) {
				this.countryArr = success.data;
			} else {
				this.popToast('error', success.message)
			}
		})
	}

	/**************************
	  List of eduactions
	****************************/
	getEducation() {
		this.commonService.callApi('getEducation', '', 'get', false, true).then(success => {
			if (success.settings.status == 1) {
				this.educationArr = success.data;


			} else {
				this.popToast('error', success.message)
			}
		})
	}

	/******************************
		No data in List
	********************************/
	dashboardList() {
		this.spinner.show();
		this.noDataList = false;
		this.getDiscoverList();
	}

	/*********************************
	 List of Users in dashboard
	**********************************/
	getDiscoverList(discArr?) {
		// if (discArr == 'discArr') {
		// 	this.data = {
		// 		page: this.page.toString(),
		// 		limit: "10",
		// 		citizen_of: this.discArr.citizen_of ? this.discArr.citizen_of : "",
		// 		user_type: this.decodeToken.data.user_type,
		// 		education: this.discArr._id ? this.discArr._id : "",
		// 		want_child: this.want_child,
		// 		i_smoke: this.i_smoke,
		// 		i_drink: this.i_drink,
		// 		age_range: this.discArr.minValue + "-" + this.discArr.maxValue
		// 	}
		// }
		// else {
		this.data = {
			page: this.page.toString(),
			limit: "10",
		}
		// }
		this.callBackSuccess = false;
		this.commonService.callApi('getDiscoverList' + this.id, this.data, 'post', true, true).then(success => {
			this.spinner.show();
			if (success.settings.status == 1) {
				this.spinner.hide()
				this.discoverListArr = success.data.list;
				if (success.data.list == "") {
					setTimeout(() => {
						this.spinner.hide();
						this.noDataList = true;
					}, 1000);
				} else {
					setTimeout(() => {
						this.spinner.hide();
						this.noDataList = false;
					}, 1000);
				}
				this.total = success.data.totalRecord;
				this.nextPage = success.data.nextPage;
				this.pageNo = success.data.nextPage;
				for (let i = 0; i < this.discoverListArr.length; i++) {
					this.crushid = this.discoverListArr[i]._id;
				}
				this.CloseFilter();
			}
			this.callBackSuccess = true;
		},
			error => {
				this.callBackSuccess = true;
				this.spinner.hide();
			})


	}
	/********************************************
			Users Listing for scrolling
	*******************************************/
	// onScrollDown() {
	// 	this.callBackSuccess = false;
	// 	if (this.data.page != undefined) {
	// 		this.data.page = (JSON.parse(this.data.page) + 1).toString();
	// 	}

	// 	this.commonService.callApi('getDiscoverList', this.data, 'post', true, true).then(success => {
	// 		if (success.settings.status == 1) {
	// 			if (this.discoverListArr.length < success.data.totalRecord) {
	// 				this.pageNo = success.data.nextPage
	// 				this.spinner.hide();
	// 				this.discoverListArr = this.discoverListArr.concat(success.data.list);
	// 			}
	// 		}
	// 		this.callBackSuccess = true;
	// 	})
	// 		, error => {
	// 			this.spinner.hide();
	// 			this.callBackSuccess = true;
	// 		}

	// }

	/********************************************
		   Users Listing for scrolling
	  *******************************************/
	@HostListener('window:scroll', ["$event"])
	onWindowScroll() {
		if (isPlatformBrowser(this.platformId)) {
			const componentPosition = this.loading.nativeElement.offsetTop - 800;
			const scrollPosition = window.pageYOffset;
			if (scrollPosition >= componentPosition) {
				if (this.nextPage !== 0 && this.callBackSuccess) {
					this.callBackSuccess = false;
					// this.spinner.show()
					// this.onScrollDown();
				}
			}
		}
	}
	/************************************************************/

	/************ Min Range values **************/
	minRangeValues(event) {
		this.discArr.minValue = event;
	}

	/************ Max Range values **************/
	maxRangeValues(event1) {
		this.discArr.maxValue = event1;
	}

	/******************************
		  clear filter
	******************************/
	clearFilter() {
		this.discArr.citizen_of = {};
		this.discArr._id = {};
		this.i_smoke = false;
		this.i_drink = false;
		this.want_child = false;
		this.discArr.maxValue = 80;
		this.discArr.minValue = 18;
		this.getDiscoverList();
	}

	/******************************
	  swipe right go to dashboard
	********************************/
	onSwipeRight() {
		this.UserDetailFlag = false;
	}

	/***********************************
	  swipe left to sent a crush request
	************************************/
	onSwipeLeft(userData, template) {
		this.swal({
			title: 'Loveternational',
			text: 'Send this user a crush request?',
			cancelButtonText: 'NO',
			confirmButtonText: 'YES',
			allowOutsideClick: false,
			showCancelButton: true,
			buttonsStyling: false
		}).then((result) => {
			if (result.value == true) {
				let data = {
					reqId: userData._id,
					userId: this.getToken('userId')
				}
				// this.getDiscoverList(this.data1)
				this.commonService.callApi('sendRequest', data, 'post', true, true).then((success) => {
					if (success.status == 1) {
						console.log("enter req");
						this.UserDetailFlag = false;
						this.getUsersData();
					}
					else if (success.settings.status == 6) {
						this.premiumMessage = success.settings.message;
						this.chargeAmount = success.data.priceToPay;
						this.modalRef = this.modalService.show(template);
					}
				})
			}
		})
	}
	/***********************************
	  swipe top to get details of user
	************************************/
	onSwipeTop(i) {
		this.showData = true;
	}

	/***********************************
	  swipe down to get video
	************************************/
	onSwipeDown(j) {
		this.UserDetailFlag = true
		if (!(j.video_url)) {
			this.videourlStatus = "No profile video found";
			this.videourl = true;
			setTimeout(() => {
				this.videourl = false;
			}, 1000);
		}
		else {
			this.video_url = j.video_url;
			this.videoEnabled = true;
		}
	}
	openPremiumModal(template1) {
		this.getClientTokens();
		this.modalRef.hide();
		this.modalRef1 = this.modalService.show(template1);
	}
	/***********************************
		  get Client Token
	  ************************************/
	getClientTokens() {
		this.commonService.callApi('client_token', '', 'get', true, false).then((success) => {
			if (success.settings.status == 1) {
				this.load = true;
				this.clientToken = success.data.token;
			}
		})
	}
	getClientTokenFunction(): Observable<any> {
		return of(this.clientToken);
	}
	/***********************************
		  User payament method
	************************************/
	createPurchase(nonce: string, chargeAmount: number) {
		const observable = from(this.commonService.callApi('checkout', { nonce: nonce, amount: chargeAmount }, 'post', false, false));
		observable.subscribe(success => {
			if (success.settings.status == 1) {
				this.paymentMessage = success.settings.message;
				this.transctionId = this.setToken("transactionId", success.data.txnId)
				setTimeout(() => {
					this.paymentMessage = ''
				}, 1000);
				this.UserDetailFlag = false;
				this.modalRef1.hide();
			}
			else {
				this.modalRef1.hide();
				this.popToast('error', success.settings.message);
			}
		})
		return observable;
	}

	/***********************************
		 hide friend list
	************************************/
	hideFriendList() {
		this.showData = false;
	}

	public touchtime = 0;
	public imageCounter = 0;
	showNextImage(userData) {
		if (this.touchtime == 0) {
			// set first click
			this.touchtime = new Date().getTime();
		} else {
			// compare first click to this click and see if they occurred within double click threshold
			if (((new Date().getTime()) - this.touchtime) < 800) {
				// double click occurred
				this.imageCounter++;
				if (this.imageCounter < 7) {
					if (this.userObject["profile_pic_" + this.imageCounter].length) {
						this.userData.profile_url = this.userObject["profile_pic_" + this.imageCounter];
					} else {
						this.imageCounter = 0;
						this.userData.profile_url = this.userObject.profile_url;
					}
				} else {
					this.imageCounter = 0;
					this.userData.profile_url = this.userObject.profile_url;
				}
				this.touchtime = 0;
			} else {
				// not a double click so set as a new first click
				this.touchtime = new Date().getTime();
			}
		}
	}

	getUsersData() {
		console.log("method called");
		let data = {
			userId: this.getToken('userId')
		}
		this.commonService.callApi('getDiscoverList', data, 'post').then((success) => {
			if (success.status == 1) {
				this.discoverListArr = success.data;
				this.spinner.hide()

			}
			else {
				this.popToast("error", success.message);
				this.spinner.hide()
			}
		})
	}
}

