import { DomSanitizer } from '@angular/platform-browser';
import { Component, OnInit, Injector, ViewChild, ElementRef } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent'
import * as RecordRTC from 'recordrtc';

declare var $: any;
declare var videojs: any;

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent extends BaseComponent implements OnInit {


  @ViewChild('AutoScroll') myScrollContainer: ElementRef;
  @ViewChild('video') video: any;
  @ViewChild('canvas') canvas: any;
  @ViewChild('videodiv') videodiv: any;

  public message_id: any;
  public player: any;
  public imageVisible: boolean = false;
  public sliceSize: any;
  public image: any;
  public videoVisible: boolean;
  public canvasType: any;
  public canvasVisible: boolean;
  public noDataChat: boolean;
  public Chat: boolean = false;
  public OptionFlag: boolean = false;
  public PageOverlayFlag: boolean = false;
  public chatData: any = [];
  public session: any;
  public message: string;
  public selectedUser: any = {};
  public userDetails: any;
  public contacts: any = [];
  public newMessage: boolean = false;
  public messageObj: {};
  public file: File;
  public base64textString: string;
  public chatConnect: boolean = false;
  public uniqueDateArray: any;
  public videoCapture: boolean = false;
  public stream: MediaStream;
  public recordRTC: any;
  public recorder: any;
  public videoUrl: string;
  public videofile: File;
  public controls: boolean = true;
  public data: any;
  public menu: boolean;
  public role: any;
  public namerole: boolean = false;
  public session_password: any = "loveInternational";
  public limit: any = 50;
  public skip: any = 0;
  public totalCount: any = 0;
  public totalunreadMessageCount: any = 0;
  constructor(inj: Injector, private sanitizer: DomSanitizer) {
    super(inj);
  }

  ngOnInit() {
    this.role = this.decodeToken.data.user_type;
    this.activatedRoute.params.subscribe(params => {
      this.data = params['data']
      if (this.data == "user") {
        this.menu = true;
        this.namerole = true;
      }
      else if (this.data == "user1") {
        this.menu = true;
        this.Chat = false;
        this.namerole = true;
      }
      else if (this.data == "attorney1") {
        this.menu = false;
        this.Chat = false;
        this.namerole = true;
      }
      else {
        this.namerole = true;
        this.menu = false;
      }
    })

    /************* Create session for chat *************/
    this.session = JSON.parse(sessionStorage.getItem('session'));
    let that = this;
    this.qBService.getProfileDetails(sessionStorage.getItem('uname'), this.session.token)
      .subscribe((data: any) => {
        this.userDetails = data;
      });
    if (sessionStorage.getItem('session')) {
      this.spinner.show();
      this.qb.chat.connect({ userId: this.session.user_id, password: this.session_password }, (err, roster) => {
        if (err) {
        } else {
          this.chatConnect = true;
          this.spinner.hide();
          if (this.getToken('groupDialog')) {
            let groupInfo = JSON.parse(this.getToken('groupDialog'));
            // this.checkAttorneyAccType(groupInfo);
            this.setToken('attorney_id', groupInfo.attorney_qb_id);
            this.qb.createSession({ email: sessionStorage.getItem('uname'), password: this.session_password }, (error, response) => {
              this.qb.chat.dialog.list({
                "type": 2,
                "occupants_ids[all]": groupInfo.selectedQbId + "," + this.session.user_id + "," + groupInfo.attorney_qb_id,
              }, (err, dialogs) => {
                this.retriveMessage(dialogs.items[0]._id, dialogs.items[0].name, dialogs.items[0].occupants_ids, dialogs.items[0].xmpp_room_jid, '', '', true);
              });
            });
          }
          else if (this.getToken('dialogGenerated')) {
            let groupInfo = JSON.parse(this.getToken('dialogGenerated'));
            this.qb.createSession({ email: sessionStorage.getItem('uname'), password: this.session_password }, (error, response) => {
              this.qb.chat.dialog.list({
                "type": 3,
                "occupants_ids[all]": this.session.user_id + "," + groupInfo.selectedQbId,
              }, (err, dialogs) => {
                this.retriveMessage(dialogs.items[0]._id, dialogs.items[0].name, dialogs.items[0].occupants_ids, '', dialogs.items[0].last_message_date_sent, '', true);
              });
            });
          }
          else {
            this.getDialog();
          }

          // Message Listenere event
          this.qb.chat.onMessageListener = (userId, msg) => {
            this.messageObj = {
              from: userId,
              body: msg.body,
              date_sent: msg.extension.date_sent,
              dialog_id: msg.extension.dialog_id,
              message_id: msg.extension.message_id,
              type: msg.type
            };
            if (userId != this.session.user_id) {
              if (msg.extension.hasOwnProperty("attachments")) {
                if (msg.extension.attachments.length > 0) {
                  var fileUID = msg.extension.attachments[0].uid;
                  this.messageObj['attachments'] = msg.extension.attachments;
                  this.messageObj['imageUrl'] = this.qb.content.privateUrl(fileUID); //- content create and upload param 'public' = true
                }
              }
              this.callListenerFunc(this.messageObj);
              // this.qBService._notificationObj.emit(this.messageObj);
            }
          };
        }
      });
    }
  }

  callListenerFunc(data) {
    if (this.Chat) {
      this.scrollToBottom();
      if (this.selectedUser) {
        if (data.from === this.selectedUser.receipientId && data.type != 'groupchat') {
          this.chatData.push({ recipient_id: data.from, message: data.body, date_sent: data.date_sent, attachments: data.attachments, imageUrl: data.imageUrl });
          this.selectedUser.last_message_date_sent = data.date_sent;
          var params = {
            messageId: data.message_id,
            userId: data.from,
            dialogId: data.dialog_id
          };
          this.qb.chat.sendReadStatus(params);
        }
        else if (data.type == 'groupchat') {
          if (!this.selectedUser[data.from]) {
            this.qb.users.get(data.from, function (error, response) {
              if (error) {
              } else {
                this.chatData.sender_name = response.full_name;
              }
            });
          }
          else {
            this.chatData.sender_name = this.selectedUser[data.from];
          }
          this.chatData.push({ recipient_id: '', sender_id: data.from, dialogId: data.dialog_id, message: data.body, date_sent: data.date_sent, attachments: data.attachments, imageUrl: data.imageUrl });

          this.selectedUser.last_message_date_sent = data.date_sent;
          var params = {
            messageId: data.message_id,
            userId: data.from,
            dialogId: data.dialog_id
          };
          this.qb.chat.sendReadStatus(params);
        }
        this.selectedUser['last_message_date_sent'] = this.chatData[(this.chatData.length) - 1]['date_sent'];
        this.uniqueDateArray = this.chatData.map(item => this.moment(item.updated_at).format('YYYY-MM-DD')).filter((value, index, self) => self.indexOf(value) === index);
      }
    }
    else {
      this.contacts.filter((o) => {
        if (o._id == data.dialog_id) {
          o.unread_messages_count = o.unread_messages_count + 1;
          o.last_message = data.body,
            o.last_message_date_sent = data.date_sent
        }
      });
    }
  }



  ngAfterViewInit() {
    setTimeout(() => {
      $('.fancybox').fancybox();
    }, 100);
  }


  OpenOption() {
    this.OptionFlag = true;
    this.PageOverlayFlag = true;
  }
  CloseOption() {
    this.OptionFlag = false;
    $('body').removeClass('open-option');
    this.PageOverlayFlag = false;
  }
  chatNoData() {
    this.spinner.show();
    this.noDataChat = false;
    this.getDialog();
  }

  /**************************************
    list of chat users
   *************************************/
  getDialog(newDialogIdCount?) {
    this.spinner.show();
    this.noDataChat = false;
    var filters = { "sort_desc": "last_message_date_sent" };
    this.qb.createSession({ email: sessionStorage.getItem('uname'), password: this.session_password }, (error, response) => {
      this.qb.chat.dialog.list({ "sort_desc": "last_message_date_sent" }, (err, resDialogs) => {
        if (err) {
        } else {
          this.spinner.hide();
          this.contacts = resDialogs.items;
          if (this.contacts == "") {
            setTimeout(() => {
              this.spinner.hide();
              this.noDataChat = true;
            }, 1000);
          }
          else {
            setTimeout(() => {
              this.spinner.hide();
              this.noDataChat = false;
            }, 1000);
          }
          resDialogs.items.filter((o) => {
            if (o.xmpp_room_jid) {
              this.qb.chat.muc.join(o.xmpp_room_jid, (resultStanza) => {
                var joined = true;
                for (var i = 0; i < resultStanza.childNodes.length; i++) {
                  var elItem = resultStanza.childNodes.item(i);
                  if (elItem.tagName === 'error') {
                    joined = false;
                  }
                }
              });
            }
            if (newDialogIdCount && o._id == newDialogIdCount) {
              o.unread_messages_count = this.totalunreadMessageCount;
            }
          });
        }
      });
    });

  }
  checkAttorneyAccType(dialogId) {
    const queryParams = "id=" + dialogId;
    this.commonService.callApi('checkAttorneyAccType?' + queryParams, '', 'get', true, true).then((success) => {
      if (success.settings.status == 0 && success.settings.statusCode == 200) {
        this.swal({
          title: 'Loveternational',
          text: success.settings.message,
          confirmButtonText: 'OK',
          allowOutsideClick: false,
          showCancelButton: false,
          buttonsStyling: false
        })
      }
    })
  }

  OpenMenu() {
    $('body').addClass('open-menu');
  }

  back() {
    this.Chat = false;
    this.getDialog();
    if (this.player) {
      this.player.record().destroy();
    }
    if (this.video) {
      this.video.nativeElement.srcObject.getVideoTracks().forEach(track => track.stop());
    }
  }
  OpenChat() {
    this.Chat = true;
  }

  MessagesFromUsers(userId, msg) {
    this.messageObj = {
      from: userId,
      body: msg.body
    };
    this.qBService._notificationObj.emit(this.messageObj);
  }
  /**
   * On message send
   *
   * @param userId: To UserId
   * @param message: composed message
   */
  onEnter(userId, message, type?) {
    if (userId && message.length) {
      this.sendMessage(userId, message, '', type);
    }
  }

  dialogId(token: any, dialogId: any) {
    throw new Error("Method not implemented.");
  }

  notifyOccupants(dialogOccupants, newDialogId) {
    dialogOccupants.forEach((itemOccupanId, i, arr) => {
      if (itemOccupanId != this.session.user_id) {
        let msg = {
          type: 'chat',
          extension: {
            notification_type: 1,
            _id: newDialogId,
          },
        };
        this.qb.chat.send(itemOccupanId, msg);
      }
    });
  }

  /**************************************
   Reterive chat history
    *************************************/
  retriveMessage(dialogId?, name?, occupants_ids?, roomId?, last_message_date_sent?, contactPhoto?, isFromDialog?) {
    if (roomId) {
      this.checkAttorneyAccType(dialogId);
    }
    this.spinner.show();
    let that = this;
    this.Chat = true;
    if (this.chatData.length < this.totalCount && this.skip == 0) {
      this.skip = 0;
      this.chatData = [];
      this.uniqueDateArray = [];
    }

    if (isFromDialog) {
      this.skip = 0;
      this.limit = 50;
      this.chatData = [];
      this.uniqueDateArray = [];
      this.totalCount = 0;
    }

    let opponentId;
    if (occupants_ids) {
      opponentId = this.qb.chat.helpers.getRecipientId(occupants_ids, this.session.user_id);
    } else {
      opponentId = this.selectedUser.receipientId;
    }
    if (roomId) {
      opponentId = '';
    }
    if (contactPhoto) {
      this.selectedUser.photo = contactPhoto;
    }
    this.selectedUser = { name: name, receipientId: opponentId, occupants_ids: occupants_ids, dialogId: dialogId, last_message_date_sent: last_message_date_sent };
    if (!this.selectedUser.receipientId) {
      var dialogJid = this.qb.chat.helpers.getRoomJidFromDialogId(dialogId);
      this.qb.chat.muc.join(dialogJid, (resultStanza) => {
        var joined = true;
        for (var i = 0; i < resultStanza.childNodes.length; i++) {
          var elItem = resultStanza.childNodes.item(i);
          if (elItem.tagName === 'error') {
            joined = false;
          }
        }
      });
    }

    var params = { chat_dialog_id: dialogId, sort_desc: 'date_sent', limit: this.limit, skip: this.skip };
    this.selectedUser['last_message_date_sent'] = "";
    this.qb.createSession({ email: sessionStorage.getItem('uname'), password: this.session_password }, (error, response) => {
      if (this.selectedUser.occupants_ids) {
        this.selectedUser.occupants_ids.filter((val) => {
          this.qb.users.get(val, function (error, response) {
            if (error) {
            } else {
              that.selectedUser["" + val + ""] = response.full_name;
            }
          });
        });
      }
      let totalItemCount;
      this.qb.chat.message.list({ chat_dialog_id: dialogId, count: 1 }, (err, messagecount) => {
        this.totalCount = messagecount.items.count;
      })
      this.qb.chat.message.list(params, (err, messages) => {
        if (messages.items.length) {
          this.spinner.hide();
          this.chatData = this.chatData.reverse();
          messages.items.filter((o) => {
            if (o.attachments.length > 0) {
              o.imageUrl = this.qb.content.privateUrl(o.attachments[0].uid);
              o.controls = true;
              o.autoplay = false;
            }
            this.chatData.push(o);
          });
          this.chatData = this.chatData.reverse();
          this.selectedUser['last_message_date_sent'] = this.chatData[(this.chatData.length) - 1]['date_sent'];
          this.uniqueDateArray = this.chatData.map(item => this.moment(item.updated_at).format('YYYY-MM-DD')).filter((value, index, self) => self.indexOf(value) === index);
          setTimeout(() => {
            $('.fancybox').fancybox();
            this.scrollToBottom();
          }, 100);
        } else {
          this.spinner.hide();
        }
      });
    });
  }


  /**************************************
   Pagination for messages
   *************************************/
  loadPrevious() {
    this.skip = this.skip + this.limit;
    this.retriveMessage(this.selectedUser.dialogId, this.selectedUser.name, this.selectedUser.receipientId, '', this.selectedUser.last_message_date_sent, '');
  }


  /**************************************
    Scroll functionality
   *************************************/
  scrollToBottom() {
    $('html, body').animate({ scrollTop: this.myScrollContainer.nativeElement.offsetTop }, 500);
  }
  /**************************************
      Send Text Messages
     *************************************/
  sendMessage(userId, message, id?, type?) {
    var dialogJid = this.qb.chat.helpers.getRoomJidFromDialogId(userId);
    this.spinner.show();
    if (type != 'chat') {
      this.qb.chat.muc.join(dialogJid, (resultStanza) => {
        var joined = true;
        for (var i = 0; i < resultStanza.childNodes.length; i++) {
          var elItem = resultStanza.childNodes.item(i);
          if (elItem.tagName === 'error') {
            joined = false;
          }
        }
      });
    }
    let currentDate: any = new Date();
    if (!id) {
      this.message_id = this.qb.chat.send(type == 'chat' ? userId : dialogJid, {
        type: type,
        body: message,
        extension: {
          save_to_history: 1
        }
      });
      this.spinner.hide();
      this.chatData.push({ recipient_id: userId, dialogId: this.selectedUser.dialogId, message: message, date_sent: parseInt(currentDate.getTime().toString().substr(0, 10)), sender_id: this.session.user_id, _id: this.message_id });
    }
    else {
      this.message_id = this.qb.chat.send(this.selectedUser.receipientId ? userId : dialogJid, message);
      this.spinner.hide();
      this.chatData.push({ recipient_id: userId, dialogId: this.selectedUser.dialogId, message: message.body, date_sent: parseInt(currentDate.getTime().toString().substr(0, 10)), attachments: message.extension.attachments, imageUrl: this.qb.content.privateUrl(message.extension.attachments[0].uid), sender_id: this.session.user_id, _id: this.message_id });
    }
    this.selectedUser['last_message_date_sent'] = this.chatData[(this.chatData.length) - 1]['date_sent'];
    this.uniqueDateArray = this.chatData.map(item => this.moment(item.updated_at).format('YYYY-MM-DD')).filter((value, index, self) => self.indexOf(value) === index);
    this.scrollToBottom();
    this.message = '';
    let messageObj = {
      body: 'Notification message',
      extension: {
        param1: "value1",
        param2: "value2"
      }
    };
    this.qb.chat.sendSystemMessage(userId, message);
  }
  /**************************************
   send With attachemnts
   *************************************/
  sendAttachment(event, isVideo?) {
    this.spinner.show();
    let inputFile: any;
    let type: any;
    if (!isVideo) {
      inputFile = event.target.files[0];
      if (inputFile.type.indexOf('video/') != -1) {
        type = 'video';
      }
      else {
        type = 'photo';
      }
    }
    else {
      if ((isVideo && isVideo == 'image')) {
        type = 'photo';
      } else {
        type = 'video';
      }
      inputFile = event;
    }

    let that = this;
    this.qb.createSession({ email: sessionStorage.getItem('uname'), password: this.session_password }, (error, response) => {
      let fileObject = new Uint8Array(inputFile);
      this.qb.content.createAndUpload({
        'public': false,
        file: inputFile,
        name: inputFile.name,
        type: inputFile.type,
        size: inputFile.size
      }, (err, response) => {
        if (err) {
        } else {
          let msg = {
            type: this.selectedUser.receipientId ? 'chat' : 'groupchat',
            body: "attachment",
            extension: {
              save_to_history: 1,
              attachments: [{ uid: response.uid, type: type }]
            }
          };
          this.spinner.hide();
          this.sendMessage(this.selectedUser.receipientId ? this.selectedUser.receipientId : this.selectedUser.dialogId, msg, 'attachemnt');
        }
      });
    });
  }
  /**************************************
    delete a particular thread
    *************************************/
  deleteMessage(message, index) {
    if (message.sender_id == this.session.user_id) {
      this.swal({
        title: 'Loveternational',
        text: 'Do you want to delete this message?',
        cancelButtonText: 'NO',
        confirmButtonText: 'YES',
        allowOutsideClick: false,
        showCancelButton: true,
        showLoaderOnConfirm: true,
        buttonsStyling: false
      }).then((result) => {
        if (result.value) {
          this.qb.createSession({ email: sessionStorage.getItem('uname'), password: this.session_password }, (error, response) => {
            var params = { force: 1 };
            this.qb.chat.message.delete(message._id, params, (err, res) => {
              if (res) {
                let deletedSuccess = JSON.parse(res);
                if (deletedSuccess.SuccessfullyDeleted) {
                  this.chatData.splice(index, 1);
                }
              } else {
                this.popToast('error', JSON.parse(err.message).message);
              }
            });
          });
        }
      })
    }
  }

  /**************************************
    Opened when click on gallery(Browse files)
   *************************************/
  openFileUpload(event) {
    this.OptionFlag = false;
    this.PageOverlayFlag = false;
    event.preventDefault();
    let element: HTMLElement = document.getElementById(
      "fileInput"
    ) as HTMLElement;
    element.click();
  }

  startCamera(isCamera?) {
    this.videoCapture = true;
    this.videoVisible = true;
    this.OptionFlag = false;
    setTimeout(() => {
      if (isCamera) {
        this.imageVisible = true;
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
          navigator.mediaDevices.getUserMedia({ video: true }).then(stream => {
            this.video.nativeElement.srcObject = stream;
          });
        }
      }
      else {
        this.imageVisible = false;
        var options = {
          controls: false,
          loop: false,
          fluid: false,
          width: 320,
          height: 240,
          controlBar: {
            volumePanel: false
          },
          plugins: {
            record: {
              audio: true,
              video: true,
              maxLength: 20,
              debug: true,
              videoEngine: 'webm-wasm',
              videoWorkerURL: 'assets/webm-wasm/webm-worker.js',
              videoWebAssemblyURL: 'webm-wasm.wasm',
              videoBitRate: 1200,
              videoFrameRate: 30
            }
          }
        };
        this.player = videojs('myVideo', options, function () {
          // print version information at startup
          videojs.log('Using video.js', videojs.VERSION,
            'with videojs-record', videojs.getPluginVersion('record'),
            'and recordrtc', RecordRTC.version);
        });
        $('.vjs-device-button').click();
      }
    }, 500);
  }

  /***************************************
       Start Recording
   *****************************************/
  startRecording(event?) {
    this.player.record().start();
  }
  /***************************************/

  /***************************************
      stopRecording
   *****************************************/
  stopRecording(stream: MediaStream) {
    this.player.record().stop();
    this.videofile = this.player.recordedData;
  }

  /***************************************
           uploading video
    *****************************************/
  uploadVideo(imageVisible) {
    if (this.videofile && !imageVisible) {
      if (this.player)
        this.player.record().destroy();
      this.videoCapture = false;
      this.sendAttachment(this.videofile, true);
    }
    else {
      if (this.file) {
        this.videoCapture = false;
        this.canvasVisible = false;
        this.sendAttachment(this.file, 'image');
        this.video.nativeElement.srcObject.getVideoTracks().forEach(track => track.stop());
      }
    }
  }
  playVideo(data, outerindex, innerindex) {
    let vidElem: any = document.getElementById(outerindex + innerindex);
    if (vidElem.requestFullscreen) {
      vidElem.requestFullscreen();
    }
    else if (vidElem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
      vidElem.webkitRequestFullscreen();
    }
    vidElem.play();
  }

  methodCancel() {
    this.videoCapture = false;
    if (this.player) {
      this.player.record().destroy();
    }
    if (this.video) {
      this.video.nativeElement.srcObject.getVideoTracks().forEach(track => track.stop());
    }
  }

  takePic(event, index?, index1?) {
    this.canvasVisible = true;
    setTimeout(() => {
      if (event == 'canvas') {
        this.canvasType = this.canvas;
        this.videoVisible = false;
        this.imageVisible = true;
        this.canvasVisible = false;
        var context = this.canvasType.nativeElement.getContext("2d").drawImage(this.video.nativeElement, 0, 0, 300, 200);
        this.image = this.canvasType.nativeElement.toDataURL("image/png");
        var block = this.image.split(";");
        var contentType = block[0].split(":")[1];
        var realData = block[1].split(",")[1];
        this.b64toBlob(realData, contentType, this.sliceSize);
      }
    }, 100);
  }
  b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;
    var byteCharacters = atob(b64Data); i
    var byteArrays = [];
    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);
      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      var byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, { type: contentType });
    this.file = new File([blob], "uploaded_file.jpg", { type: contentType, lastModified: Date.now() });
  }

  /***************************************
           uploading video
    *****************************************/
  uploadImage() {
    if (this.file) {
      this.videoCapture = false;
      this.canvasVisible = false;
      this.spinner.show();
      this.sendAttachment(this.file);
    }
  }


  /*****************************************
          Remove tokens
   *******************************************/
  ngOnDestroy() {
    this.removeToken('groupDialog');
    this.removeToken('dialogGenerated');
    this.removeToken('attorney_id');
  }

}
