import { Component, OnInit, Injector, ViewChild, ElementRef } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent'
import { OwlOptions } from 'ngx-owl-carousel-o';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { esLocale } from 'ngx-bootstrap';
import * as RecordRTC from 'recordrtc';
declare var jQuery: any;
declare var $: any;
import { Location } from '@angular/common'
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent extends BaseComponent implements OnInit {
  public Edit: boolean;
  public userArr: any = {};
  public countryArr: Array<any> = [];
  public educationArr: Array<any> = [];
  public have_child: any;
  public want_child: any;
  public want_child_1: any;
  public i_smoke: any;
  public i_smoke_1: any;
  public i_drink: any;
  public birthday: any;
  public birthdayYear: any;
  public image: any;
  public user: any = {};
  public selectedDate: any;
  public date: any;
  public newCurrentDate: any;
  public OptionFlag: boolean = false;
  public profileImage: any;
  public videoType: any;
  public sliceSize: any;
  public file: any;
  public canvasVisible: boolean = true;
  public VideoOpenOptionFlag: boolean = false;
  public VideoRemoveOptionFlag: boolean = false;
  public imageDataVisible: boolean = true;
  public currentImageIndex: any;
  public FirstOptionFlag: boolean = false;
  public FirstRemoveFlag: boolean = false;
  public noVideodisplay: boolean = true;
  public tempArray: any = [];
  public profile_pic_1: string;
  public profile_pic_2: string;
  public profile_pic_3: string;
  public profile_pic_4: string;
  public profile_pic_5: string;
  public profile_pic_6: string;
  public videoPreview: any;
  public recorder: any;
  public recordRTC: any = {};
  public stream;
  public videofile: any;
  public url1: any = {};
  public id: any;
  public file1: string;
  public usernameData: string;
  public usernamecheck: boolean;
  public nameMessage: string;
  public nameError: boolean;
  public aboutCheck: boolean;
  public aboutMe: string;
  public educationErr: string;
  public educationCheck: boolean;
  public cancel: boolean = true
  public uploadCancel: boolean = true;
  public role: any;
  public currentDate: Date = new Date();
  public birthdayMessage: any;
  public birthdayError: boolean = false;
  public birthdayVal: any;
  public tempArr = [];
  public videoUrl: any;

  /************************************
   6 upload static images
   ***********************************/
  public SecondImageArray: Array<any> = [
    { profile2: 'assets/images/no-image-user.png' },
    { profile2: 'assets/images/no-image-user.png' },
    { profile2: 'assets/images/no-image-user.png' },
    { profile2: 'assets/images/no-image-user.png' },
    { profile2: 'assets/images/no-image-user.png' },
    { profile2: 'assets/images/no-image-user.png' },
  ]
  myList: any = [];
  public genderList: any = [
    { name: 'Select Gender', value: 'Select Gender' },
    { name: 'Male', value: 'Male' },
    { name: 'Female', value: 'Female' },
    { name: 'Other', value: 'Other' },
  ];
  customOptions: OwlOptions = {
    loop: true,
    dots: false,
    autoWidth: true,
    nav: false,
    
  };

  constructor(inj: Injector, private location: Location) {
    super(inj)
  }
  // ngDoCheck() {
  //   this.userArr.video_url = this.getToken('video_url');
  // }
  ngOnInit() {
    this.role = this.decodeToken.data.user_type;
    this.id = this.decodeToken.data._id;
    this.getCountryList();
    this.getEducationList();
    this.getUserProfile();
  }

  OpenMenu() {
    $('body').addClass('open-menu');
  }
  back() {
    this.Edit = false;
  }
  edit() {
    this.Edit = true;
    this.getUserProfile()
  }
  ngAfterViewInit() {
    setTimeout(() => {
      $('.fancybox').fancybox();
    }, 100)
  }
  /******************************
        Country List
   *********************************/
  getCountryList() {
    this.commonService.callApi('getCountryList/user', '', 'get', false, true).then(success => {
      if (success.settings.status == 1) {
        this.countryArr = success.data;
      } else {
        this.popToast('error', success.message)
      }
    })
  }

  /*********************************
          Education List
   ***********************************/
  getEducationList() {
    this.commonService.callApi('getEducation', '', 'get', false, true).then(success => {
      if (success.settings.status == 1) {
        this.educationArr = success.data;
      } else {
        this.popToast('error', success.message)
      }
    })
  }

  changeStatus(event, key) {
    this.userArr[key] = event;
  }

  /*********************************************
              Date Validation Message 
  *****************************************/
  onValueChange(event) {
    if (event) {
      let x = this.currentDate.getFullYear() - event.getFullYear();
      if (x < 18) {
        this.user.birthday = '';
        this.birthdayMessage = 'Only 18 years above can select the birth date'
        this.birthdayError = true;
        setTimeout(() => {
          this.birthdayError = false;
        }, 1000);
      }
    }
  }


  /***************************************
     First profile Drop Down  functions
   *****************************************/
  OpenOption() {
    this.OptionFlag = true;
  }
  CloseOption() {
    this.OptionFlag = false;
    $('body').removeClass('open-option');
  }

  /***************************************
      6 profile images  functions
    *****************************************/
  FirstOpenOption(index) {
    this.currentImageIndex = index;
    this.FirstOptionFlag = true;
    this.FirstRemoveFlag = false;
  }
  FirstCloseOption() {
    this.FirstOptionFlag = false;
    this.FirstRemoveFlag = false;
  }
  FirstOpenFile(event, type?) {
    this.VideoOpenOptionFlag = false;
    event.preventDefault();
    let element = document.getElementById((type ? 'profile3' : 'profile2') + this.currentImageIndex).click();
  }


  /***************************************
        Gallery image Uploading  functions
   *****************************************/

  openfile(event: any) {
    event.preventDefault();
    let element: HTMLElement = document.getElementById('profile') as HTMLElement;
    element.click();
  }
  fileChangeEvent(event: any, profile, index, file): void {
    if (profile == 'profile') {
      this.profileImage = event.target.files[0];
      this.image = URL.createObjectURL(this.profileImage);
      this.OptionFlag = false;
    }
    if (profile == 'profile2') {
      this.tempArray[index] = event.target.files[0];
      for (var i = 0; i < this.tempArray.length; i++) {
        this.profile_pic_1 = this.tempArray[0];
        this.profile_pic_2 = this.tempArray[1];
        this.profile_pic_3 = this.tempArray[2];
        this.profile_pic_4 = this.tempArray[3];
        this.profile_pic_5 = this.tempArray[4];
        this.profile_pic_6 = this.tempArray[5];
      }
      this.url1 = event.target.files[0];
      this.url1["profile2"] = URL.createObjectURL(this.url1);
      this.SecondImageArray[this.currentImageIndex] = this.url1;
      if (this.tempArray.length) {
        var fd1 = new FormData();
        fd1.append('profile_pic_1', this.profile_pic_1 ? this.profile_pic_1 : " ");
        fd1.append('profile_pic_2', this.profile_pic_2 ? this.profile_pic_2 : " ");
        fd1.append('profile_pic_3', this.profile_pic_3 ? this.profile_pic_3 : " ");
        fd1.append('profile_pic_4', this.profile_pic_4 ? this.profile_pic_4 : " ");
        fd1.append('profile_pic_5', this.profile_pic_5 ? this.profile_pic_5 : " ");
        fd1.append('profile_pic_6', this.profile_pic_6 ? this.profile_pic_6 : " ");
        this.commonService.callApi('addUpdateImages/' + this.id, fd1, 'post', true, false).then(success => {
          if (success.settings.status == 1) {
          }
          else {
            this.popToast('error', success.settings.message)
          }
        })
      }
    }
    else if (profile == 'profile3') {
      this.videofile = event.target.files[0];
      if (this.videofile) {
        let file = new FormData();
        file.append('video_url', this.videofile);
        this.spinner.show();
        this.commonService.callApi("uploadProfileVideo/" + this.id, file, 'post', true, true).then(success => {
          if (success.settings.status == 1) {
            this.spinner.hide();
            this.videoPreview = success.data.video_url_thumb;
          }
        })
      }
    }
    this.OptionFlag = false;
    this.FirstOptionFlag = false;
  }

  /*************************************
          Edit Profile Form
   *****************************************/
  submitEditUserForm(profileForm, user) {
    if (user.full_name == '') {
      this.nameMessage = 'Please enter your fullname'
      this.nameError = true;
      setTimeout(() => {
        this.nameError = false;
      }, 1000);
    }
    if (user.username == '') {
      this.usernameData = 'Please enter username'
      this.usernamecheck = true;
      setTimeout(() => {
        this.usernamecheck = false;
      }, 1000);
    }
    else if (user.about_me == '') {
      this.aboutMe = 'Please enter some description about you'
      this.aboutCheck = true;
      setTimeout(() => {
        this.aboutCheck = false;
      }, 1000);
    }
    else if (!profileForm.form.controls.education_id.valid) {
      this.educationErr = 'Please select your highest education.'
      this.educationCheck = true;
      setTimeout(() => {
        this.educationCheck = false;
      }, 1000);
    }
    else if (profileForm.valid) {
      var fd = new FormData();
      if (this.profileImage) {
        fd.append('profile_url', this.profileImage)
      }
      else {
        fd.append('profile_url', this.image)
      }
      fd.append('full_name', user.full_name);
      fd.append('username', user.username);
      fd.append('about_me', user.about_me);
      fd.append('birthday', this.birthday);
      fd.append('live_in', user.live_in_id);
      fd.append('education', user.education_id)
      fd.append('citizen_of', user.citizen_of_id);
      fd.append('gender', user.gender);
      fd.append('interested_in', user.interested_in);
      fd.append('want_child', this.userArr.want_child);
      fd.append('i_smoke', this.userArr.i_smoke);
      fd.append('i_drink', this.userArr.i_drink);
      fd.append('have_child', this.userArr.have_child);
        this.spinner.show();
      this.commonService.callApi('addEditProfile/' + this.id, fd, 'post', true, true).then(success => {
        if (success.settings.status == 1) {
            this.spinner.hide()
          this.setToken('profile_url', success.data.profile_url);
          this.setToken('full_name', success.data.full_name);
          this.userArr = {};
          this.getUserProfile();
          this.Edit = false;
        }
        else {
          this.birthdayVal = success.settings.message;
          setTimeout(() => {
            this.birthdayVal = "";
            this.spinner.hide()
          }, 1000);
        }
      })
      this.OptionFlag = false;
      this.FirstOptionFlag = false;
    }
    else {
    }
  }

  /***************************************
     video dropdown
   *****************************************/
  VideoOpenOption() {
    this.VideoOpenOptionFlag = true;
  }
  VideoRemoveOption() {
    this.VideoOpenOptionFlag = false;
  }
  VideoCloseOption() {
    this.VideoOpenOptionFlag = false;
  }

  /********************************
       Get the Profile details
   ************************************/
  getUserProfile() {
    this.commonService.callApi('getUserProfile/' + this.id, '', 'get', true, true).then(success => {
      if (success.settings.status == 1) {
        this.userArr = success.data;
        this.setToken('profile_url', success.data.profile_url);
        let tempArray = [];
        tempArray.push(success.data.profile_pic_1, success.data.profile_pic_2, success.data.profile_pic_3, success.data.profile_pic_4, success.data.profile_pic_5, success.data.profile_pic_6)
        this.image = success.data.profile_url;
        if (success.data.video_url == "") {
          this.noVideodisplay = false
        }
        this.videoPreview = success.data.video_url_thumb;
        for (let index = 0; index < this.SecondImageArray.length; index++) {
          for (let j = 0; j < tempArray.length; j++) {
            const element = tempArray[j];
            if (index == j) {
              this.SecondImageArray[index].profile2 = element;
            }
          }
        }
        this.birthday = new Date(success.data.birthday);
        this.birthdayYear = this.birthday.getFullYear();
      } else {
        this.popToast('error', success.message)
      }
    })
  }
}
