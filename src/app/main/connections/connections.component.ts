import { Component, OnInit, Injector, Input, ViewChild, ElementRef, HostListener } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent'
import { TabDirective } from 'ngx-bootstrap/tabs';
import { Location } from '@angular/common'
import { TabsetComponent } from 'ngx-bootstrap';
import { isPlatformBrowser } from '@angular/common';
declare var $: any;
@Component({
  selector: 'app-connections',
  templateUrl: './connections.component.html',
  styleUrls: ['./connections.component.css']
})
export class ConnectionsComponent extends BaseComponent implements OnInit {

  @ViewChild('staticTabs') staticTabs: TabsetComponent;
  @ViewChild('loading') loading: ElementRef;

  public UserDetailFlag = false;
  public noDone: boolean = false;
  public video_url: any;
  public conectionsLoader = false;
  public noDataInterest: boolean = false;
  public noDataCrush: boolean = false;
  public noDataRequest: boolean = false;
  public userData: any = {};
  public interestArr: Array<any> = [];
  public crushArr: Array<any> = [];
  public requestedArr: Array<any> = [];
  public successMessageStatus: any;
  public successMessage: boolean = false;
  public showData: boolean = false;
  public videourl: boolean = false;
  public videourlStatus: any;
  public EditFlag = false;
  public EditBtnFlag = false;
  public OpenBtnFlag = false;
  public likeButton: boolean = false;
  public req_status: any;
  public UserVideoFlag = false;
  public total: any;
  public data: any = {};
  public nextPage: any;
  public role: any;
  public video: boolean = false;
  public pageNo: any;
  public page: any = 1;
  public crushEditBtn;
  public intrstEditBtn;
  public reqBtn;
  public userObject: any = {};
  public touchtime = 0;
  public imageCounter = 0;
  public callBackSuccess: boolean = true;

  @Input() friendsData;

  constructor(inj: Injector, private Location: Location) {
    super(inj);
  }

  ngOnInit() {
    // this.role = this.decodeToken.data.user_type;
    // this.spinner.show()
    setTimeout(() => {
      this.getInterestList();
    }, 1000);

  }
  /************ open menu bar **************/
  OpenMenu() {
    $('body').addClass('open-menu');
  }
  /*************************
    back to connections
  ***************************/
  back() {
    this.UserDetailFlag = false;
    this.UserVideoFlag = false;
    this.likeButton = false;
    this.imageCounter = 0;
    this.userData.profile_url = this.userObject.profile_url;
  }
  back1(tabs) {
    this.UserDetailFlag = true;
    this.video = false;
    if (tabs.Interests == true) {
      this.likeButton = false;
    }
    else if (tabs.Requested == true) {
      this.likeButton = false;
    }
    else {
      this.likeButton = true;
    }
  }
  /*****************************
    open modal of user details
  ******************************/
  UserDetailOpen(tabs, i) {
    this.UserDetailFlag = true;
    this.userData = i;
    Object.assign(this.userObject, ...this.userData);
    if (tabs == 'interest') {
      this.likeButton = false;
      this.onSwipeTop(this.userData)
    }
    else if (tabs == 'crush') {
      this.likeButton = true
    }
    else {
      this.likeButton = false
    }
  }

  /*****************************
    close modal of user details
  ******************************/
  UserDetailClose(userData, like) {
    this.swal({
      title: 'Loveternational',
      text: (like == 'close') ? 'You have chosen to DECLINE this crush request. Proceed?' : 'You have chosen to ACCEPT this crush request. You and this user will now become mutual interests. Proceed?',
      cancelButtonText: 'NO',
      confirmButtonText: 'YES',
      allowOutsideClick: false,
      showCancelButton: true,
      buttonsStyling: false
    }).then((result) => {
      if (result.value == true && like == 'close') {
        this.req_status = "rejected";
        let data: any = {
          ids: [userData._id],
          req_status: this.req_status
        }
        // data.userId = this.getToken('userId')
        this.userRequest(data, this.tabs);
        this.UserDetailFlag = false;
      }
      if (result.value == true && like == 'like') {
        this.req_status = "accepted";
        let data = {
          ids: [userData._id],
          req_status: this.req_status
        }
        this.userRequest(data, this.tabs);
        this.UserDetailFlag = false;
      }
    })
  }
  /*************** edit button *******************/

  EditBtn(edit, tabs) {
    if (tabs.Crush) {
      this.crushEditBtn = true;
      this.intrstEditBtn = false;
      this.reqBtn = false
    }
    this.EditFlag = true;
    this.OpenBtnFlag = true;
    this.EditBtnFlag = true;
  }

  /***************************************
    Cancel the selected requested users
  *****************************************/
  public object: any;
  DoneBtn(tabs) {
    if (Object.keys(this.selectedItem).length == 0) {
      this.OpenBtnFlag = false;
      this.EditFlag = false;
      this.EditBtnFlag = false;
      this.noDone = true;
    }
    if (Object.keys(this.selectedItem).length) {
      this.swal({
        title: 'Loveternational',
        text: (tabs.Requested == true) ? 'Are you sure,you want to cancel your request for selected users?' :
          (tabs.Interests == true) ? 'Are you sure,you want to remove selected users from your interest list?' : 'Are you sure, you want to reject selected users request?',
        cancelButtonText: 'NO',
        confirmButtonText: 'YES',
        allowOutsideClick: false,
        showCancelButton: true,
        buttonsStyling: false
      }).then((result) => {
        if (result.value == true) {
          this.req_status = "rejected"
          let data = {
            ids: this.selectedItem,
            req_status: this.req_status
          }
          this.userRequest(data, tabs);
        }
        else {
          this.OpenBtnFlag = true;
          this.EditFlag = true;
        }
      })
    }
  }
  /******************************
      List of Interested Users
    *******************************/
  public tabs: any = {};
  getInterestList() {
    // this.spinner.show();
    this.noDataInterest = false;
    this.OpenBtnFlag = false;
    this.EditBtnFlag = false;
    this.EditFlag = false;
    this.tabs.Interests = true;
    this.tabs.Crush = false;
    this.tabs.Requested = false;
    this.data = {
      page: this.page.toString(),
      limit: "4"
    }
    this.commonService.callApi('getInterestList', this.data, 'post', true, true).then(success => {
      this.interestArr = success.data.list;
      this.total = success.data.totalRecord;
      this.conectionsLoader = false;
      this.nextPage = success.data.nextPage;
      this.pageNo = success.data.nextPage;
      if (success.data.list == "") {
        this.noDone = false;
        if (this.nextPage !== 0) {
          this.onWindowScroll()
        }
        setTimeout(() => {
          this.spinner.hide();
          this.noDataInterest = true;
        }, 1000);
      }
      else {
        this.noDone = true;
        setTimeout(() => {
          this.spinner.hide();
          this.noDataInterest = false;
        }, 500);
      }

    },
      error => {
        this.spinner.hide();
      })
  }

  /******************************
         List of Crushes
   *******************************/
  getCrushList() {
    // this.spinner.show();
    this.noDataCrush = false;
    this.OpenBtnFlag = false;
    this.EditBtnFlag = false;
    this.EditFlag = false;
    this.tabs.Interests = false;
    this.tabs.Crush = true;
    this.tabs.Requested = false;
    this.data = {
      page: this.page.toString(),
      limit: "4",
      userId: this.getToken('userId')
    }
    //this.callBackSuccess = false;
    this.commonService.callApi('crushList', this.data, 'post', true, true).then(success => {
      console.log(success, "successs===>");

      this.crushArr = success.data;
      this.total = success.data.totalRecord;
      this.nextPage = success.data.nextPage;
      this.conectionsLoader = false;
      this.pageNo = success.data.nextPage;
      if (success.data.list == "") {
        this.noDone = false;
        if (this.nextPage !== 0) {
          this.onWindowScroll()
        }
        setTimeout(() => {
          this.spinner.hide();
          this.noDataCrush = true;
        }, 1000);
      }
      else {
        this.noDone = true;
        setTimeout(() => {
          this.spinner.hide();
          this.noDataCrush = false;
        }, 1000);
      }
      //this.callBackSuccess = true;
    },
      error => {
        // this.callBackSuccess = true;
        this.spinner.hide();
      })
  }
  /******************************
        List of requested users
    *******************************/
  getRequestedList() {
    // this.spinner.show();
    this.noDataRequest = false;
    this.OpenBtnFlag = false;
    this.EditBtnFlag = false;
    this.EditFlag = false;
    this.tabs.Interests = false;
    this.tabs.Crush = false;
    this.tabs.Requested = true;
    this.data = {
      page: this.page.toString(),
      limit: "4",
      userId: this.getToken('userId')
    }

    this.commonService.callApi('requestList', this.data, 'post', true, true).then(success => {
      this.requestedArr = success.data;
      this.conectionsLoader = false;
      this.total = success.data.totalRecord;
      this.nextPage = success.data.nextPage;
      this.pageNo = success.data.nextPage;
      if (success.data.list == "") {
        this.noDone = false;
        if (this.nextPage !== 0) {
          this.onWindowScroll()
        }
        setTimeout(() => {
          this.spinner.hide();
          this.noDataRequest = true;
        }, 1000);
      }
      else {
        this.noDone = true;
        setTimeout(() => {
          this.spinner.hide();
          this.noDataRequest = false;
        }, 1000);
      }

    },

      error => {

        this.spinner.hide();
      })

  }
  /*********** no interest data ***************/
  interestData() {
    this.spinner.show();
    this.noDataInterest = false;
    this.getInterestList();
  }
  /*********** no crush data ***************/
  crushData() {
    this.spinner.show();
    this.noDataCrush = false;
    this.getCrushList();
  }
  /*********** no request data ***************/
  requestData() {
    this.spinner.show();
    this.noDataRequest = false;
    this.getRequestedList();
  }

  /*************************************
    select the users to cancel request
  **************************************/
  public selectedItem: Array<any> = [];
  selectUsers(j) {
    let count = 0;
    let index;
    for (let k = 0; k < this.selectedItem.length; k++) {
      if (j._id == this.selectedItem[k]) {
        count++;
        index = k;
      }
    }
    if (count) {
      this.selectedItem.splice(index, 1)
    } else {
      this.selectedItem.push(j._id)
    }
  }
  /**********************************
   swipe right  go back to connetions
  *************************************/
  onSwipeRight() {
    this.UserDetailFlag = false;
    this.likeButton = false;
  }
  /**********************************
   swipe top  to get user details
  *************************************/
  onSwipeTop(userData) {
    this.UserDetailFlag = true;
    this.showData = true;
  }
  /**********************************
   swipe down  to get user video
  *************************************/
  onSwipeDown(j) {
    if (!(j.video_url)) {
      this.video = false;
      this.videourlStatus = "No profile video found";
      this.videourl = true;
      setTimeout(() => {
        this.videourl = false;
      }, 1000);
    }
    else {
      this.likeButton = false;
      this.video_url = j.video_url;
      this.video = true;
    }
  }
  /********************************
      Like user request in crush tab
    *********************************/
  userRequest(data, option?) {
    data.userId = this.getToken('userId');
    data.key = "request"
    this.commonService.callApi('acceptRejectRequest', data, 'post', true, true).then((success) => {
      if (success.status == 1) {
        this.successMessageStatus = success.message;
        this.successMessage = true;
        setTimeout(() => {
          this.successMessage = false;
        }, 1000);
        option.Interests == true ? this.getInterestList() : option.Requested == true ? this.getRequestedList() : this.getCrushList();
      }
      else {
        this.swal({
          title: 'Loveternational',
          text: success.message,
          confirmButtonText: 'OK',
          allowOutsideClick: false,
          showCancelButton: false,
          buttonsStyling: false
        })
      }
    })
  }
  /*********************************
     Hide Friends List
   ************************************/
  hideFriendList() {
    this.showData = false;
  }

  /**********************************************
    double click on profile pic six images scroll
   ***********************************************/
  showNextImage(userData) {
    if (this.touchtime == 0) {
      // set first click
      this.touchtime = new Date().getTime();
    } else {
      // compare first click to this click and see if they occurred within double click threshold
      if (((new Date().getTime()) - this.touchtime) < 800) {
        // double click occurred
        this.imageCounter++;
        if (this.imageCounter < 7) {
          if (this.userObject["profile_pic_" + this.imageCounter].length) {
            this.userData.profile_url = this.userObject["profile_pic_" + this.imageCounter];
          } else {
            this.imageCounter = 0;
            this.userData.profile_url = this.userObject.profile_url;
          }
        } else {
          this.imageCounter = 0;
          this.userData.profile_url = this.userObject.profile_url;
        }
        this.touchtime = 0;
      } else {
        // not a double click so set as a new first click
        this.touchtime = new Date().getTime();
      }
    }
  }

  /********************************************
          InterestUsers Listing for scrolling
     *******************************************/
  onScrollDown(tabs?) {

    if (this.data.page) {
      this.data.page = (JSON.parse(this.data.page) + 1).toString();
    }

    if (this.tabs.Interests == true) {
      if (this.pageNo) {
        this.commonService.callApi('getInterestList', this.data, 'post', true, true).then(success => {
          if (success.settings.status == 1) {
            if (this.interestArr.length < success.data.totalRecord) {
              this.conectionsLoader = false;
              this.interestArr = this.interestArr.concat(success.data.list);
              this.pageNo = success.data.nextPage;
              this.total = success.data.totalRecord;
            }
          }
        })

      }
      error => {
        this.spinner.hide();

      }
    }
    if (this.tabs.Crush == true) {
      if (this.pageNo) {
        this.commonService.callApi('getCrushList', this.data, 'post', true, true).then(success => {
          if (success.settings.status == 1) {
            this.conectionsLoader = false;
            if (this.crushArr.length < success.data.totalRecord) {
              this.crushArr = this.crushArr.concat(success.data.list);
              this.total = success.data.totalRecord;
              this.pageNo = success.data.nextPage;
            }
          }
        })

      }
      error => {
        this.spinner.hide();

      }
    }
    if (this.tabs.Requested == true) {

      if (this.pageNo) {
        this.commonService.callApi('getRequestList', this.data, 'post', true, true).then(success => {
          if (success.settings.status == 1) {
            this.conectionsLoader = false;
            if (this.requestedArr.length < success.data.totalRecord) {
              this.requestedArr = this.requestedArr.concat(success.data.list);
              this.pageNo = success.data.nextPage;

            }
            this.total = success.data.totalRecord;
          }
        })

      }
      error => {
        this.spinner.hide();

      }
    }
  }
  /********************************************
        Users Listing for scrolling
   *******************************************/
  @HostListener('window:scroll', ["$event"])
  onWindowScroll(tabs?) {
    if (isPlatformBrowser(this.platformId)) {
      const componentPosition = this.loading.nativeElement.offsetTop - 800;
      const scrollPosition = window.pageYOffset;
      if (scrollPosition >= componentPosition && this.callBackSuccess) {
        if (this.nextPage !== 0 && !this.conectionsLoader) {
          this.conectionsLoader = true;
          this.onScrollDown(tabs);
        }
      }
    }
  }
}
