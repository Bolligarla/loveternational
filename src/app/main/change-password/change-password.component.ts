import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent'
import { from } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent extends BaseComponent implements OnInit {

  public changepwd = {};
  public backroute: any;
  public passtype: string = "password";
  public show: boolean = false;
  public oldPassMsg: any;
  public NewPassMsg: any;
  public oldPassMsgError: boolean = false;
  public NewPassMsgError: boolean = false;
  public NewPassMsgLengthError: boolean = false;
  public errorMessageStatus: boolean = false;
  public successMessageStatus: any;
  public successMessage: boolean = false;
  public errorMessage: any;
  
  constructor(inj: Injector) {
    super(inj);
    this.activatedRoute.params.subscribe(params => {
      this.backroute = params['data'];
    })
  }
  ngOnInit() {
  }

  /***********************************
     show password in text/password
   **************************************/
  showpass() {
    this.show = !this.show;
    if (this.show) {
      this.passtype = "text";
    } else {
      this.passtype = "password";
    }
  }

  /************************************
    submit change password
  **************************************/
  submitChangePassword(form, changepwd) {
    if (changepwd.oldPassword == undefined) {
      // this.oldPassMsg = 'Please enter your old password'
      this.oldPassMsgError = true;
      setTimeout(() => {
        this.oldPassMsgError = false;
      }, 1000);
    }
    else if (changepwd.newPassword == undefined) {
      this.NewPassMsg = 'Please enter your password'
      this.NewPassMsgError = true;
      setTimeout(() => {
        this.NewPassMsgError = false;
      }, 1000);
    }
    else if (changepwd.oldPassword.length) {
      if (changepwd.newPassword.length < 8) {
        this.NewPassMsg = 'The password must be of minimum length 8 characters'
        this.NewPassMsgError = true;
        setTimeout(() => {
          this.NewPassMsgError = false;
        }, 1000);
      }
    }
    if (form.valid && changepwd.newPassword.length >= 8) {
      let data = {
        newPassword: changepwd.newPassword,
        oldPassword: changepwd.oldPassword,
        user_id: this.decodeToken.data._id
      }
      this.spinner.show();
      this.commonService.callApi('changePassword', data, 'post').then(success => {
        if (success.settings.status == 1) {
          this.spinner.hide();
          if (!this.successMessage) {
            this.successMessageStatus = success.settings.message;
            this.successMessage = true;
            setTimeout(() => {
              this.successMessage = false;
            }, 1000);
          }
          setTimeout(() => {
            if (this.backroute == 'attorney') {
              this.router.navigate(["/main/attorney/attorney-setting"]);
            }
            else {
              this.router.navigate(["/main/setting"]);
            }
          }, 1000);
        }
        else {
          setTimeout(() => {
            this.spinner.hide();
            if (!this.errorMessageStatus) {
              this.errorMessage = success.settings.message
              this.errorMessageStatus = true;
              setTimeout(() => {
                this.errorMessageStatus = false;
              }, 1000);
            }
          }, 500);
        }
      })
    }
  }

  /*************************************
        Navigate to setiings
  ***************************************/
  back() {
    if (this.backroute == 'attorney') {
      this.router.navigate(["/main/attorney/attorney-setting"]);
    }
    else {
      this.router.navigate(["/main/setting"]);
    }
  }
}
