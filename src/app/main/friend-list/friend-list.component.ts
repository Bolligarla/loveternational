import { Component, OnInit, Injector, TemplateRef, Input, Output, EventEmitter } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-friend-list',
  templateUrl: './friend-list.component.html',
  styleUrls: ['./friend-list.component.css']
})
export class FriendListComponent extends BaseComponent implements OnInit {

  public session: any;
  public ReportFlag = false;
  public PageOverlayFlag = false;
  public FriendListFlag = false;
  public modalRef: BsModalRef;
  public friendsData: any = {};
  public report: any = {};
  public birthday: Date;
  public birthdayYear: number;
  public attorney: boolean = false;
  public loginUserAcc_type: any;
  public reportabuse: boolean = true;
  public session_password: any = "loveInternational";
  public transactionId: any;

  @Input() data;
  @Input() data1;
  @Input() attorneyData;
  @Output() hideFriendList = new EventEmitter();

  constructor(inj: Injector, private modalService: BsModalService) {
    super(inj);
  }
  ngDoCheck() {
    if (this.getToken('transactionId')) {
      this.transactionId = this.getToken('transactionId');
    }
  }
  ngOnInit() {
    this.loginUserAcc_type = this.decodeToken.data.acc_type
    /*******************************************
     data getting from connections and dahboard
     ******************************************/
    if (this.data) {
      this.friendsData = this.data;
      this.attorney = false;
    }
    else
      if (this.data1) {
        this.friendsData = this.data1;
        if (this.data1.status == "interested") {
          this.attorney = true;
        }
        else if (this.data1.status == "requested") {
          this.attorney = false;
        }
        else {
          this.attorney = false;
        }
      }
      else {
        this.friendsData = this.attorneyData;
        this.reportabuse = false;
      }
    this.birthday = new Date(this.friendsData.birthday);
    this.birthdayYear = this.birthday.getFullYear();
    setTimeout(() => {
      this.FriendListFlag = true;
    }, 300);
    this.session = JSON.parse(sessionStorage.getItem('session'));
    if (sessionStorage.getItem('session')) {
      this.qb.chat.connect({ userId: this.session.user_id, password: this.session_password }, (err, roster) => {
        if (err) {
        }
      });
    }
  }
  /****************************
   open  modal
 *******************************/
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  /****************************
    open report modal
  *******************************/
  ReportFlagOpen() {
    this.ReportFlag = true;
    this.PageOverlayFlag = true;
  }
  /****************************
    close report modal
  *******************************/
  ReportFlagClose() {
    this.PageOverlayFlag = false;
    this.ReportFlag = false;
  }

  /******************************************
   swipe down go to connections or dashboard
  *******************************************/
  onSwipeDown() {
    this.hideFriendList.emit();
  }

  /******************************************
    submit the abusing the report
  *******************************************/
  reportAbuse(report) {
    let data = {
      abuse_id: this.friendsData._id,
      report: report
    }
    this.spinner.show();
    this.commonService.callApi("reportAbuse", data, 'post', true, true).then((success) => {
      if (success.settings.status == 1) {
        setTimeout(() => {
          this.ReportFlag = false;
          this.PageOverlayFlag = false;
          this.spinner.hide();
        }, 1000);
      }
      else {
        setTimeout(() => {
          this.spinner.hide()
        }, 1000);
      }
    })
  }

  /************************************************************************************
   when payment is completed then go to attorney list otherwise it shows pop message
   ************************************************************************************/
  attorneyList(friendsData) {
    if (this.transactionId && friendsData.acc_type == 'premium') {
      this.createGroupChat();
    }
    else if (friendsData.acc_type == 'free' || this.loginUserAcc_type == 'free') {
      this.swal({
        title: 'Loveternational',
        text: 'It looks like your partner is not a premium member. Both of you need to be premium members to use the attorney chat function.',
        confirmButtonText: 'OK',
        allowOutsideClick: false,
        showCancelButton: false,
        buttonsStyling: false
      })
    }
    else {
      this.createGroupChat();
    }
  }

  createGroupChat() {
    this.setToken('intrestedFriendUserId', this.friendsData._id);
    this.setToken('intrestedFriendQBId', this.friendsData.quickbloxId);
    this.setToken('intrestedFriendQBFullName', this.friendsData.full_name);
    if (this.friendsData.attorney_acc_type == "free") {
      const queryParams = "attorney_id=" + this.friendsData.attorney_id + "&user_id=" + this.friendsData.user_id;
      this.commonService.callApi("sendEmailReminder?" + queryParams, "", "get", true, true).then((success) => {
        if (success.settings.status == 1) {
          this.router.navigate(['/main/attorney-list']);
        }
      })
    }
    else {
      this.setToken('intrestedFriendUserId', this.friendsData._id);
      this.setToken('intrestedFriendQBId', this.friendsData.quickbloxId);
      this.setToken('intrestedFriendQBFullName', this.friendsData.full_name);
      if (this.friendsData.grp_dialg_id) {
        let groupDialog = {
          dialogId: this.friendsData.grp_dialg_id,
          selectedQbId: this.friendsData.quickbloxId,
          selectedUserName: this.friendsData.full_name, attorney_qb_id: this.friendsData.attorney_qbId,
        }
        this.setToken('groupDialog', JSON.stringify(groupDialog));
        this.router.navigate(['/main/chat', 'user']);
      } else {
        this.router.navigate(['/main/attorney-list'])
      }
    }
  }


  /******************************************
   redirecting to chat detail
  *******************************************/
  redirectToChatDetail() {
    if (!this.friendsData.quickbloxId) {
      this.popToast('error', 'Something Went Wrong, Please try again');
      return false;
    }
    this.setToken('intrestedFriendUserId', this.friendsData._id);
    this.setToken('intrestedFriendQBId', this.friendsData.quickbloxId);
    this.setToken('intrestedFriendQBFullName', this.friendsData.full_name);
    if (this.friendsData.dialg_id) {
      let groupDialog = {
        dialogId: this.friendsData.dialg_id,
        selectedQbId: this.getToken('intrestedFriendQBId'),
        selectedUserName: this.getToken('intrestedFriendQBFullName'),
      }
      this.setToken('dialogGenerated', JSON.stringify(groupDialog));
      this.router.navigate(['/main/chat', 'user']);
    } else {
      var params = {
        type: 3,
        occupants_ids: [this.friendsData.quickbloxId, this.session.user_id],
        name: this.friendsData.full_name + "," + this.getToken('full_name')
      };
      this.qb.createSession({ email: sessionStorage.getItem('uname'), password: this.session_password }, (error, response) => {
        this.qb.chat.dialog.create(params, (err, createdDialog) => {
          if (err) {
          } else {
            let data = {
              int_id: this.friendsData._id,
              dialg_id: createdDialog._id
            }
            this.commonService.callApi('addQBDialogId', data, 'post', true, true).then((success) => {
              if (success.settings.status == 1) {
                let groupDialog = {
                  dialogId: createdDialog._id,
                  selectedQbId: this.getToken('intrestedFriendQBId'),
                  selectedUserName: this.getToken('intrestedFriendQBFullName'),
                }
                this.setToken('dialogGenerated', JSON.stringify(groupDialog));
                this.router.navigate(['/main/chat', 'user']);
              }
            })
          }
        });
      });
    }
  }
}
