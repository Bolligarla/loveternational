import { Component, OnInit, Injector, HostListener, ViewChild, ElementRef } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent'
import { Options } from 'ng5-slider';
import { isPlatformBrowser } from '@angular/common';
declare var $: any;
@Component({
  selector: 'app-attorney-list',
  templateUrl: './attorney-list.component.html',
  styleUrls: ['./attorney-list.component.css']
})
export class AttorneyListComponent extends BaseComponent implements OnInit {

  @ViewChild('loading') loading: ElementRef;

  public session: any;
  public minValue: number = 0;
  public maxValue: number = 50;
  public attorney: any = {};
  public countryArr: Array<any> = [];
  public stateArr: Array<any> = [];
  public attorneyArr: Array<any> = [];
  public attorneyData: any = {};
  public ratingReview: any = {}
  public ratingUsers: Array<any> = [];
  public totalcount: any;
  public ratings: boolean = false;
  public noDataList: boolean = false;
  public id: any;
  public data: any = {};
  public nextPage: any;
  public total: any;
  public data1: any;
  public pageNo;
  public page: any = 1;
  public AttorneyList: boolean = false;
  public session_password: any = "loveInternational";
  public callBackSuccess: boolean = true;
  options: Options = {
    floor: 0,
    ceil: 50,
    step: 1
  };


  constructor(inj: Injector) {
    super(inj)
  }

  ngOnInit() {
    this.attorney.maxValue = 50;
    this.attorney.minValue = 0;
    this.attorneyList();
    this.session = JSON.parse(sessionStorage.getItem('session'));
    if (sessionStorage.getItem('session')) {
      this.qb.chat.connect({ userId: this.session.user_id, password: this.session_password }, (err, roster) => {
        if (err) {
        } else {
        }
      });
    }
  }

  back() {
    this.AttorneyList = false;
  }

  /*****************************
    open attorney's list data
   ******************************/
  OpenAttorneyList(i) {
    this.AttorneyList = true;
    this.attorneyData = i;
    this.getAttorneyRatingView(this.attorneyData._id);
  }

  /*********************************
    get the attorney's using filter
   ***********************************/
  OpenFilter() {
    $('body').addClass('open-filter');
    this.getCountryList();
  }
  CloseFilter() {
    $('body').removeClass('open-filter');
  }

  /*****************************
        Country list
   *********************************/
  getCountryList() {
    this.commonService.callApi('getCountryList/attroney', '', 'get', false, true).then(success => {
      if (success.settings.status == 1) {
        this.countryArr = success.data;
        this.attorney.country_id = this.countryArr[0]._id;
        this.getStateList();
      }
    })
  }
  changeCountry(event) {
    this.attorney.state_id = '';
    this.getStateList();
  }

  /*****************************
       states list
  *********************************/
  getStateList() {
    this.commonService.callApi('getStateListByCountryId/' + this.attorney.country_id, '', 'get', false, true).then(success => {
      if (success.settings.status == 1) {
        this.stateArr = success.data;
        this.attorney.state_id = this.stateArr[0]._id;
      }
    })
  }

  /************ Min Range values **************/
  minRangeValues(event) {
    this.attorney.minValue = event;
  }

  /************ Max Range values **************/
  maxRangeValues(event1) {
    this.attorney.maxValue = event1;
  }


  /******************************
         clear filter
   ******************************/
  clearFilter() {
    this.attorney.country_id = this.countryArr[0]._id;
    this.attorney.state_id = this.stateArr[0]._id;
    this.attorney.maxValue = 50;
    this.attorney.minValue = 0;
    this.attorneyList();
  }

  /*************************************
        Attorney list
   **************************************/
  attorneyList(attorney?) {
    if (attorney == 'attorney') {
      this.data = {
        page: "1",
        limit: "10",
        country_id: this.attorney.country_id,
        state_id: this.attorney.state_id,
        experience: this.attorney.minValue + '-' + this.attorney.maxValue
      }
    }
    else {
      this.data = {
        page: this.page.toString(),
        limit: "10"
      }
    }
    this.callBackSuccess = false;
    this.commonService.callApi('getApprovedAttorneyList', this.data, 'post', true, true).then((success) => {
      if (success.settings.status == 1) {
        if (success.data.list == "") {
          setTimeout(() => {
            this.spinner.hide();
            this.noDataList = true;
          }, 1000);
        } else {
          setTimeout(() => {
            this.spinner.hide();
            this.noDataList = false;
          }, 1000);
        }
        this.attorneyArr = success.data.list;
        this.total = success.data.totalRecord;
        this.nextPage = success.data.nextPage;
        if (this.nextPage !== 0) {
          this.onWindowScroll();
        }
      }

      this.CloseFilter();
      this.callBackSuccess = true;
    },
      error => {
        this.callBackSuccess = true;
        this.spinner.hide();
      })
  }

  /*********************************
    get Attorney Ratings
   **********************************/
  getAttorneyRatingView(id) {
    let data = {
      page: this.page.toString(),
      limit: "10",
      id: id
    }
    this.commonService.callApi('getAttorneyRatings', data, 'post', true, true).then((success) => {
      if (success.settings.status == 1) {
        this.ratingReview = success.data.data;
        this.ratingUsers = success.data.list;
        this.totalcount = success.data.totalRecord;
        if (success.data.list.length == 0) {
          this.ratings = true;
        }
        else {
          this.ratings = false;
        }
      }
    })
  }

  /************************************
     Initiate the Group Chat
   ***************************************/
  intiateGroupChat(attoneryqbId, attorneyName, userId) {
    var params = {
      type: 2,
      occupants_ids: [this.getToken('intrestedFriendQBId'), this.session.user_id, attoneryqbId],
      name: this.getToken('intrestedFriendQBFullName') + "," + this.getToken('full_name') + "," + attorneyName,
    };
    this.qb.createSession({ email: sessionStorage.getItem('uname').toLowerCase(), password: this.session_password }, (error, response) => {
      this.qb.chat.dialog.create(params, (err, createdDialog) => {
        if (err) {
        } else {
          let data = {
            int_id: this.getToken('intrestedFriendUserId'),
            attorney_id: userId,
            grp_dialg_id: createdDialog._id
          }
          this.commonService.callApi('addQBDialogId', data, 'post', true, true).then((success) => {
            if (success.settings.status == 1) {
              let groupDialog = {
                dialogId: createdDialog._id,
                selectedQbId: this.getToken('intrestedFriendQBId'),
                selectedUserName: this.getToken('intrestedFriendQBFullName'), attorney_qb_id: attoneryqbId,
              }
              this.setToken('groupDialog', JSON.stringify(groupDialog));
              this.router.navigate(['/main/chat', 'user']);
            }
          })
        }
      });
    });
  }

  /********************************************
		   Users Listing for scrolling
	  *******************************************/
  @HostListener('window:scroll', ["$event"])
  onWindowScroll() {
    if (isPlatformBrowser(this.platformId)) {
      const componentPosition = this.loading.nativeElement.offsetTop - 800;
      const scrollPosition = window.pageYOffset;
      if (scrollPosition >= componentPosition) {
        if (this.nextPage !== 0 && this.callBackSuccess) {
          this.callBackSuccess = false;
          this.spinner.show()
          this.onScrollDown();
        }
      }
    }
  }
  /************************************************************/

  /********************************************
    Users Listing for scrolling
*******************************************/
  onScrollDown() {
    this.callBackSuccess = false;
    if (this.data.page != undefined) {
      this.data.page = (JSON.parse(this.data.page) + 1).toString();
    }
    this.data.page = this.data.page;
    if (this.pageNo) {
      this.commonService.callApi('getApprovedAttorneyList', this.data, 'post', true, true).then((success) => {
        if (success.settings.status == 1) {
          if (this.attorneyArr.length < success.data.totalRecord) {
            this.pageNo = success.data.nextPage
            this.spinner.hide();
            this.attorneyArr = this.attorneyArr.concat(success.data.list);
          }
        }
        this.callBackSuccess = true;
      }),
        error => {
          this.spinner.hide();
          this.callBackSuccess = true;
        }

    } else {
      this.spinner.hide();
    }
  }

  /**********************************
     no data in attorneylist
   *********************************/
  attorneyDataList() {
    this.spinner.show();
    this.noDataList = false;
    this.attorneyList();
  }
}
