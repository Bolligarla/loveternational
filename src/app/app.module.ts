import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatPaginatorModule, MatSlideToggleModule } from '@angular/material';
import { SocialLoginModule, AuthServiceConfig, FacebookLoginProvider } from "angular-6-social-login";



// Plugins
import { NgSelectModule } from '@ng-select/ng-select';
import { CKEditorModule } from 'ng2-ckeditor'; //1. https://github.com/chymz/ng2-ckeditor //2. https://ckeditor.com/ckeditor-4/download/
import { CarouselModule } from 'ngx-owl-carousel-o';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgxBraintreeModule } from 'ngx-braintree';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { PaginationModule, TabsModule } from 'ngx-bootstrap';
import { BnDatatableModule } from './common/bn-datatable/bn-datatable.module'
import { ImageCropperModule } from 'ngx-image-cropper';
import { TinymceModule } from 'angular2-tinymce';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ClipboardModule } from 'ngx-clipboard';
import { MatTooltipModule } from '@angular/material';
import { ImageUploadModule } from "angular2-image-upload";
import { UiSwitchModule } from 'ngx-toggle-switch';
import { FileUploadModule } from 'ng2-file-upload';
import { Ng5SliderModule } from 'ng5-slider';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { AutosizeModule } from 'ngx-autosize';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AlertModule } from 'ngx-bootstrap';
import { RatingModule } from 'ng-starrating';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG, } from '@angular/platform-browser';
import { ToastrModule } from 'ngx-toastr';
import { TimeagoModule } from 'ngx-timeago';
import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar';
import 'hammerjs/hammer';

declare var Hammer: any;

// Common
import { BaseComponent } from './common/commonComponent';
import { SafeHtml } from "./common/safeHtml";
import { OrderByPipe } from './common/order';
import { CustomTime, checkDate, CustomPipe, limitTextPipe } from './common/customPipe.pipe';
import { CanLoginActivate, CanAuthActivate } from './common/auth.gaurd';
import { Broadcaster } from './common/broadcaster';

// public pages
import { HomeComponent } from './public/home/home.component';
import { LoginComponent } from './public/login/login.component';
import { SignUpComponent } from './public/sign-up/sign-up.component';
import { ResetPasswordComponent } from './public/reset-password/reset-password.component';
import { ForgotPasswordComponent } from './public/forgot-password/forgot-password.component';

// main pages
import { MainComponent } from './main/main.component';
import { DashboardComponent } from './main/dashboard/dashboard.component';
import { ChangePasswordComponent } from './main/change-password/change-password.component';
import { NotificationComponent } from './main/notification/notification.component';
import { AttorneyTermsComponent } from './reusable/attorney-terms/attorney-terms.component';
import { UserTermsComponent } from './reusable/user-terms/user-terms.component';
import { UserFaqComponent } from './reusable/user-faq/user-faq.component';
import { UserPolicyComponent } from './reusable/user-policy/user-policy.component';
import { SocialLoginComponent } from './reusable/social-login/social-login.component';
import { UpdateProfileComponent } from './public/update-profile/update-profile.component';
import { MenuComponent } from './public/menu/menu.component';
import { ConnectionsComponent } from './main/connections/connections.component';
import { ProfileComponent } from './main/profile/profile.component';
import { ChatComponent } from './main/chat/chat.component';
import { SettingComponent } from './main/setting/setting.component';
import { FriendListComponent } from './main/friend-list/friend-list.component';
import { AttorneyComponent } from './attorney/attorney.component';
import { AttorneyDashboardComponent } from './attorney/attorney-dashboard/attorney-dashboard.component';
import { AttorneyProfileComponent } from './attorney/attorney-profile/attorney-profile.component';
import { AttorneyUpdateProfileComponent } from './attorney/attorney-update-profile/attorney-update-profile.component';
import { AttorneyRatingComponent } from './attorney/attorney-rating/attorney-rating.component';
import { AttorneySettingComponent } from './attorney/attorney-setting/attorney-setting.component';
import { AttorneyNotificationComponent } from './attorney/attorney-notification/attorney-notification.component';
import { AttorneyListComponent } from './main/attorney-list/attorney-list.component';
import { UserAttorneyRatingComponent } from './main/user-attorney-rating/user-attorney-rating.component';
export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider("419599542253766")
      },
    ]
  );
  return config;
}
export class MyHammerConfig extends HammerGestureConfig {
  buildHammer(element: HTMLElement) {
    let mc = new Hammer(element, {
      touchAction: "auto",
      inputClass: Hammer.SUPPORT_POINTER_EVENTS ? Hammer.PointerEventInput : Hammer.TouchInput,
      recognizers: [
        [Hammer.Swipe, {
          direction: Hammer.DIRECTION_ALL
        }]
      ]
    });
    return mc;
  }
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    MainComponent,
    BaseComponent,
    SafeHtml,
    OrderByPipe,
    CustomTime, checkDate, CustomPipe, limitTextPipe,
    CanLoginActivate,
    CanAuthActivate,
    DashboardComponent,
    ResetPasswordComponent,
    ChangePasswordComponent,
    NotificationComponent,
    SignUpComponent,
    SocialLoginComponent,
    UserPolicyComponent,
    AttorneyTermsComponent,
    UserFaqComponent,
    UserTermsComponent,
    ForgotPasswordComponent,
    UpdateProfileComponent,
    MenuComponent,
    ConnectionsComponent,
    ProfileComponent,
    ChatComponent,
    SettingComponent,
    FriendListComponent,
    AttorneyComponent,
    AttorneyDashboardComponent,
    AttorneyProfileComponent,
    AttorneyUpdateProfileComponent,
    AttorneyRatingComponent,
    AttorneySettingComponent,
    AttorneyNotificationComponent,
    AttorneyListComponent,
    UserAttorneyRatingComponent
  ],
  imports: [
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    }),
    MalihuScrollbarModule.forRoot(),
    ImageUploadModule.forRoot(),
    TabsModule.forRoot(),
    AlertModule.forRoot(),
    InfiniteScrollModule,
    NgxChartsModule,
    ClipboardModule,
    MatTooltipModule,
    FileUploadModule,
    UiSwitchModule,
    CarouselModule,
    NgxSpinnerModule,
    BsDatepickerModule.forRoot(),
    RatingModule,
    Ng5SliderModule,
    AutosizeModule,
    TimeagoModule.forRoot(),
    TinymceModule.withConfig({
      plugins: ["image", "code", "lists", "textcolor", "media", "link", "table"],
      menubar: "",
      min_height: 500,
      toolbar:
        "undo redo | alignleft, aligncenter, alignright, alignjustify | bold italic underline | link | code | image | media | link | numlist bullist | forecolor backcolor | table"
    }),
    BnDatatableModule,
    NgSelectModule,
    PaginationModule.forRoot(),
    MatPaginatorModule,
    MatSlideToggleModule,
    LoadingBarHttpClientModule,
    SweetAlert2Module.forRoot(),
    BrowserAnimationsModule,
    CKEditorModule,
    FormsModule,
    SocialLoginModule,
    BrowserModule.withServerTransition({ appId: "universal-demo-v5" }),
    HttpClientModule,
    NgxBraintreeModule,
    BrowserTransferStateModule,
    ImageCropperModule,
    RouterModule.forRoot(
      [
        { path: "", redirectTo: "home", pathMatch: "full" },
        {
          path: "sign-in",
          component: LoginComponent,
          // canActivate: [CanLoginActivate],
          pathMatch: "full"
        },
        {
          path: "sign-up",
          component: SignUpComponent,
          // canActivate: [CanLoginActivate],
          pathMatch: "full"
        },
        {
          path: "home",
          component: HomeComponent,
          // canActivate: [CanLoginActivate],
          pathMatch: "full"
        },
        {
          path: "resetPassword",
          component: ResetPasswordComponent,
          pathMatch: "full"
        },
        {
          path: "forgotpassword",
          component: ForgotPasswordComponent,
          pathMatch: "full"
        },
        {
          path: "",
          component: MenuComponent,
          // canActivate: [CanAuthActivate],
          pathMatch: "full"
        },
        {
          path: "main",
          component: MainComponent,
          // canActivate: [CanAuthActivate],
          children: [
            { path: "", redirectTo: "dashboard", pathMatch: "full" },

            {
              path: "dashboard",
              component: DashboardComponent,
              pathMatch: "full"
            },
            {
              path: "profile",
              component: ProfileComponent,
              pathMatch: "full"
            },
            {
              path: "friend-list",
              component: FriendListComponent,
              pathMatch: "full"
            },
            {
              path: "attorney-list",
              component: AttorneyListComponent,
              pathMatch: "full"
            },
            {
              path: "user-attorney-rating/:id/:data",
              component: UserAttorneyRatingComponent,
              pathMatch: "full"
            },
            {
              path: "chat/:data",
              component: ChatComponent,
              pathMatch: "full"
            },
            {
              path: "setting",
              component: SettingComponent,
              pathMatch: "full"
            },
            {
              path: "connections",
              component: ConnectionsComponent,
              pathMatch: "full"
            },
            {
              path: "notification",
              component: NotificationComponent,
              pathMatch: "full"
            },
            {
              path: "update-profile",
              component: UpdateProfileComponent,
              pathMatch: "full"
            },
            {
              path: "change-password/:data",
              component: ChangePasswordComponent,
              pathMatch: "full"
            },
            {
              path: "attorney",
              component: AttorneyComponent,
              children: [
                { path: "", redirectTo: "attorney-update-profile", pathMatch: "full" },
                {
                  path: "attorney-dashboard",
                  component: AttorneyDashboardComponent,
                  pathMatch: "full"
                },
                {
                  path: "attorney-profile",
                  component: AttorneyProfileComponent,
                  pathMatch: "full"
                },
                {
                  path: "attorney-rating/:id",
                  component: AttorneyRatingComponent,
                  pathMatch: "full"
                },
                {
                  path: "attorney-update-profile",
                  component: AttorneyUpdateProfileComponent,
                  pathMatch: "full"
                },
                {
                  path: "attorney-setting",
                  component: AttorneySettingComponent,
                  pathMatch: "full"
                },
                {
                  path: "attorney-notification",
                  component: AttorneyNotificationComponent,
                  pathMatch: "full"
                },
              ]
            },

            { path: "**", redirectTo: "dashboard", pathMatch: "full" }
          ]
        },

        { path: "**", redirectTo: "home", pathMatch: "full" }
      ],
      { useHash: true }
    ),
    // ServiceWorkerModule.register('../../ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    CanLoginActivate,
    CanAuthActivate,
    Broadcaster,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },
    {
      // hammer instantion with custom config
      provide: HAMMER_GESTURE_CONFIG,
      useClass: MyHammerConfig,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
