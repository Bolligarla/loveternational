import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttorneyTermsComponent } from './attorney-terms.component';

describe('AttorneyTermsComponent', () => {
  let component: AttorneyTermsComponent;
  let fixture: ComponentFixture<AttorneyTermsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttorneyTermsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttorneyTermsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
