import { Component, OnInit, Injector, Input } from '@angular/core';
import { BaseComponent } from './../../common/commonComponent';
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angular-6-social-login';
import { updateLocale } from 'ngx-bootstrap/chronos/locale/locales';
import { SignUpComponent } from '../../public/sign-up/sign-up.component';
import * as JWT from 'jwt-decode';

@Component({
  selector: 'app-social-login',
  templateUrl: './social-login.component.html',
  styleUrls: ['./social-login.component.css']
})
export class SocialLoginComponent extends BaseComponent implements OnInit {

  @Input('register') register;

  public profileComplete: any;
  public facebook: boolean = false;

  constructor(private socialAuthService: AuthService, inj: Injector) {
    super(inj);
  }

  ngOnInit() {
    if (this.register == 'userRegister') {
      this.facebook = true;
    }
    else {
      this.facebook = false;
    }
  }
  /************************************
    Social login
*************************************/
  socialSignIn(socialPlatform: string) {
    console.log("method called");

    let socialPlatformProvider;
    if (socialPlatform == "facebook") {
      console.log("123");
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
      console.log("123========>", socialPlatformProvider);
    }
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log("123");

        console.log(userData, "userdata");

        let data = {
          full_name: userData.name,
          fbId: userData.id,
          email: userData.email.toLowerCase(),
          device_type: "web",
          device_id: "abcd",
          user_type: "user"
        }
        this.commonService.callApi("userRegister", data, "post", false, true).then(success => {
          if (success.settings.status == 1) {
            this.qb.createSession({ email: userData.email, password: success.data.qbps }, (error, response) => {
              if (response) {
                var params = { email: userData.email, password: success.data.qbps };
                this.qb.login(params, (err, result) => {
                  if (result.id) {
                    this.spinner.hide();
                    sessionStorage.setItem('session', JSON.stringify(response));
                    sessionStorage.setItem('uname', userData.email);
                  }
                });
              } else {
              }
            });
            this.setToken('full_name', success.data.full_name);
            this.setToken('profile_url', success.data.profile_url);
            this.setToken('accessToken', success.data.token);
            if (success.data.fbId) {
              this.setToken('fbId', success.data.fbId)
            }
            if (this.getToken('accessToken')) {
              this.decodeToken = JWT(this.getToken('accessToken'));
            }
            if (this.decodeToken.data.is_profile_complete == false) {
              this.router.navigate(["/main/update-profile"]);
            }
            else {
              this.router.navigate(["/main/dashboard"]);
            }
          }
          else if (success.settings.status == 0) {
            this.popToast('error', success.settings.message)
          }
        })
      }
    );
  }
}
