import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-user-faq',
  templateUrl: './user-faq.component.html',
  styleUrls: ['./user-faq.component.css']
})
export class UserFaqComponent implements OnInit {
  @Input() faq;
  constructor() { }

  ngOnInit() {
  }

}
